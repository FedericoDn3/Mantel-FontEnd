import React ,{useState,useEffect} from "react";
import {
  BrowserRouter as Router, Route ,Navigate, Routes
} from "react-router-dom";
// -------------------------------------------------
import { TailSpin } from "react-loader-spinner"
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import useToken from "./Components/useToken"
import Login from  "./Components/Login"
import Landing from "./Components/Landing"
import Register from "./Components/Register"
import HeaderCli from "./Components/Cliente/HeaderClient"
import HeaderProd from "./Components/Prod/HeaderProd"
import HeaderAdm from "./Components/Admin/HeaderAdmin"
import Player from "./Components/Contenido/LivePlayer"
import ChatL from "./Components/Contenido/Chat"
import LandingCli from "./Components/Cliente/LandingClient"
import VistaContenido from "./Components/Contenido/VistaContenido"
//import AltaContenido from "./Components/Contenido/AltaContenido"
import AltaProductor from "./Components/Prod/AltaProductor"
import Perfil from "./Components/Cliente/Perfil"
import AdminContenidoProobedor from "./Components/Prod/AdminContenidoProobedor"
import PPVListado from "./Components/Cliente/PPVListado";
import ListaBuscar from "./Components/Contenido/ListaBuscar";
import BuscarSeries from "./Components/Contenido/BuscarSeries";
import AdminContenidoAdmin from "./Components/Admin/AdminContenidoAdmin";
import AdminContenidoReportado from "./Components/Admin/AdminContenidoReportado";
import HabilitarProductor from "./Components/Admin/HabilitarProductor";
import AltaContenido from "./Components/Prod/Altas/AltaContenido";
import AltaEvento from "./Components/Prod/Altas/AltaEvento";
import AltaPelicula from "./Components/Prod/Altas/AltaPelicula";
import AltaSerie from "./Components/Prod/Altas/AltaSerie";
import MiPerfil from "./Components/Cliente/Perfil";
import AdministrarProductor from "./Components/Admin/AdministrarProductor";
import Pagaos from "./Components/Pagaos";
import PagoProd from "./Components/Admin/PagoProd";




function App() {

  const { token, setToken } = useToken()
  const [user, setUser] = useState(null)
  const [pago, setPago] = useState(false)




   useEffect(() => {
    if(token)
<<<<<<< HEAD
      ('Hay token')
      (token)
=======
      console.log('Hay token')
      console.log(token)
>>>>>>> main
      setUser(token)
      setPago(sessionStorage.getItem('Pago'))
      //fetchInitObject().then(data => setUser(data))
      fetchInitObject()
    }, [token]) 

    const fetchInitObject =async () => {
<<<<<<< HEAD
            ('Hey')
      const data = sessionStorage.getItem("Peps")
            (data)
            ('Hey2')
=======
            console.log('Hey')
      const data = sessionStorage.getItem("Peps")
            console.log(data)
            console.log('Hey2')
>>>>>>> main
      return data
    }


  //  const fetchInitObject =async () => {
  //   const token = sessionStorage.getItem('ftoken')
  //   const x =token.replace(/['"]+/g, '')
  //   const settings = {
  //       method: 'GET',
  //       headers: {
  //       Accept: 'application/json',
  //       'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8',
  //       Authorization:'Bearer ' +  x
  //       }
  //   }
  //   const fetchResponse =await fetch('http://127.0.0.1:5000/login', settings).then()
  //   const data =await fetchResponse.json()
  //   sessionStorage.setItem('id', JSON.stringify(data.id))
  //   sessionStorage.setItem('name', JSON.stringify(data.name))
  //   return data
  // }

  if(!token) {
    return (
      <Router>
      <Routes>
      <Route path="*"element={<Navigate to={"/"} />}/>
      <Route path="/" element={<Navigate to="/Mantel" />} />
      <Route path="/Mantel" element={<><Login setToken={setToken}/><Landing /></>} /> 
      <Route path="/Registro" element={<Register />} />
      <Route path="/RegistroProobedor" element={<AltaProductor />} />   
      </Routes>
      </Router>

    )
  } else if(user!=null){
    if (user.tipoUsuario === "Administrador") {
    // if (user.tipoUsuario === "Cliente") {
      return (
        <Router>
        <Routes>
        <Route path="*"element={<Navigate to={"/Admin"} />}/>
        <Route path="/" element={<Navigate to="/Admin" />} />
        <Route path="/Admin" element={<><HeaderAdm /><AdministrarProductor /></>} />    
        <Route path="/AdminContenidoBaja" element={
          <div><HeaderAdm /><AdminContenidoReportado /></div> 
         } />
        <Route path="/AltaPelicula" element={
          <div><HeaderProd /><AltaPelicula /></div> 
         } />
         <Route path="/PagoProd" element={
          <div><HeaderProd /><PagoProd /></div> 
         } />
         <Route path="/AdminContenidoAlta" element={
          <div><HeaderAdm /><AdminContenidoAdmin /></div> 
         } />  
        <Route path="/HabilitarProductor" element={
          <div><HeaderAdm /><HabilitarProductor /></div> 
         } />
        </Routes>
        </Router>
      )
    }else if(user.tipoUsuario === "Productor" && !user.bloqueo){
    // }else if(user.tipoUsuario === "Cliente"){
      return (
        <Router>
        <Routes>
        <Route path="*"element={<Navigate to={"/Productor"} />}/>
        <Route path="/" element={<Navigate to="/Productor" />} />
        <Route path="/Productor" element={<><HeaderProd /><AdminContenidoProobedor /></>} />  
        <Route path="/AltaSerie" element={
          <div><HeaderProd /><AltaSerie /></div> 
         } />
         <Route path="/AltaPelicula" element={
          <div><HeaderProd /><AltaPelicula /></div> 
         } />
         <Route path="/AltaEvento" element={
          <div><HeaderProd /><AltaEvento /></div> 
         } />
         <Route path="/AltaContenido" element={
          <div><HeaderProd /><AltaContenido /></div> 
         } />
        </Routes>
        </Router>
      ) 
    }else if(user.tipoUsuario === "Productor" && user.bloqueo ){
      return (
        <div>
          <p>Disculpe las molestias, si esta viendo esto, contacte un administrador</p>
        </div>
      )
    }else if(user.tipoUsuario === "Cliente" && (user.subscripcion.activo || pago)){
    // }else if(user.tipoUsuario === "Productor"){
      return (

        <Router>
          <div><HeaderCli /></div>
        <Routes>
        <Route path="*"element={<Navigate to={"/Cliente"} />}/>
        <Route path="/" element={<Navigate to="/Cliente" />} />

        <Route path="/Mantel/:titulo/:id" element={
        <>
          <div className=" md:flex md:h-auto md:justify-around"><Player /><ChatL/></div></>
         } />
        <Route path="/Cliente" element={
          <div className="bg-black">
            <LandingCli />
            </div>  
         } />
         <Route path="/Peliculas" element={
          <div><ListaBuscar /></div> 
         } />
         <Route path="/BuscarSeries/:query" element={
          <div><BuscarSeries /></div> 
         } />  
        <Route path="/Peli/:id" element={
          <div><VistaContenido /></div> 
         } />
         <Route path="/Perfil" element={
          <div><MiPerfil /></div> 
         } />
         <Route path="/Buscar/:query" element={
          <div><ListaBuscar /></div> 
         } />
         <Route path="/Eventos" element={
          <div><PPVListado /></div> 
         } />  
        <Route path="A" element={
          <div><AltaContenido /></div> 
         } />
        <Route path="B" element={
          <div><AltaProductor /></div> 
         } />   
          <Route path="D" element={
          <div><HabilitarProductor /></div> 
         } /> 
           <Route path="E" element={
          <div><Perfil /></div> 
         } /> 
        </Routes>
        </Router>
      )  
    } else if(user.tipoUsuario === "Cliente" && (!user.subscripcion.activo || pago) ){
      return (
        <Router>
        <Routes>
        <Route path="*"element={<Navigate to={"/"} />}/>
        <Route path="/" element={<Navigate to="/Pagaos" />} />
        <Route path="/Pagaos" element={<Pagaos />} />
        </Routes>
        </Router>
      )
    }else {
      return(<div className="py-8 px-8 max-w-sm mx-auto bg-white rounded-xl space-y-2 sm:py-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-6">
      <TailSpin color="#1AA7EC" height={80} width={80} Timeout={5000} />
      </div>
      )
    }
  }else{
    //return(<div className="py-8 px-8 max-w-sm mx-auto bg-white rounded-xl space-y-2 sm:py-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-6">Aquí me vez</div>)
     return(<div className="py-8 px-8 max-w-sm mx-auto bg-white rounded-xl space-y-2 sm:py-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-6">
      <TailSpin color="#1AA7EC" height={80} width={80} Timeout={5000} />
     </div>)
  }
}

export default App;


