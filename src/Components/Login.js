import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {Helmet} from "react-helmet";
import swal from 'sweetalert';
import {
    UsersIcon,
  } from '@heroicons/react/solid'
import Logo from '../Images/MantelL.png'

    //Esto 
    async function loginUser(credentials) {
        //Headers y body para el fetch.
        const settings = {
            method: 'POST',
            headers: {
            Accept: 'application/json',
            'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
            },
            body: new URLSearchParams({
                Mail:credentials.id,
                Pass:credentials.password
            })
        }        
        return fetch('http://localhost:8080/Java2022_war/Prueba/Login',settings).then(data => data.json())    
    }   
    
    export default function Login({setToken}) {
            const [id, setId] = useState();
            const [password, setPassword] = useState();
            //const [seguirConectado,setSeguirConectado] = useState()
        
            const handleSubmit = async (e) => {
                e.preventDefault();
                const tokens = await loginUser({id,password})
                const token = tokens
                setToken(token)
                sessionStorage.setItem('IdUsu', JSON.stringify(token.id))
                sessionStorage.setItem('Tipo', JSON.stringify(token.tipoUsuario))
                sessionStorage.setItem('Nombre',JSON.stringify(token.nombre))
                swal({
                    title:"Login Exitoso.",
                    icon: "success",
                    button: false,
                    timer:500
                })

            // setTimeout(() => {    
            //     window.location.href = 'http://localhost:3000/'}, 3000);

            }
    
    return (
        <div className="max-w-2xl mx-auto">

        <Helmet>
        <script src="https://unpkg.com/@themesberg/flowbite@1.2.0/dist/flowbite.bundle.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/@themesberg/flowbite@1.2.0/dist/flowbite.min.css" />
        </Helmet>
        <button className="group flex text-SecC focus:ring-1 focus:ring-red-500 font-medium rounded-lg text-medium px-5 py-2.5 text-center bg-MainC
        hover:bg-SecC
        " type="button" data-modal-toggle="authentication-modal">
        Login &nbsp; <UsersIcon className='bg-MainC
        h-5 w-5 group-hover:bg-SecC
        '/>
        </button>


        <div id="authentication-modal" aria-hidden="true" className="hidden overflow-x-hidden overflow-y-auto fixed h-modal md:h-full top-4 left-0 right-0 md:inset-0 z-50 justify-center items-center">
            <div className="relative w-full max-w-md px-4 h-full md:h-auto">
                
                <div className="bg-white rounded-lg shadow relative">
                    <div className="flex justify-end p-2">
                        <button type="button" className="text-MainC
                        bg-transparent hover:bg-SecC
                        hover:text-red-700 rounded-lg text-medium p-1.5 ml-auto inline-flex items-center " data-modal-toggle="authentication-modal">
                            <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>  
                        </button>
                    </div>
                    <form className="space-y-6 px-6 lg:px-8 pb-4 sm:pb-6 xl:pb-8" onSubmit={handleSubmit}>
                        <img src={Logo} alt="Logo" className='flex place-items-center align-middle' style={{height:'5vh'}} />
                        <h3 className="flex text-xl font-medium text-MainC
                        dark:text-SecC
                        ">Bienvenidos a Mantel!</h3>

                        <div>
                            <label htmlFor="email" className="text-medium font-medium text-MainC
                            block mb-2 dark:text-MainC
                            ">email</label>
                            <input type="email" name="email" id="email" onChange={e => setId(e.target.value)} className=" bg-SecC
                            border border-MainC
                            text-red-800 sm:text-medium rounded-lg focus:ring-MainC
                            focus:border-MainC
                            block w-full p-2.5 dark:bg-SecC
                            dark:border-MainC
                            dark:placeholder-red-800 dark:text-red-800" placeholder="nombre@mail.com" required={true}/>
                        </div>
                        <div>
                            <label htmlFor="password" className="text-medium font-medium text-MainC
                            block mb-2 dark:text-MainC
                            ">password</label>
                            <input type="password" name="password" id="password" placeholder="password" onChange={e => setPassword(e.target.value)} className="bg-SecC
                            border border-MainC
                            text-red-800 sm:text-medium rounded-lg focus:ring-MainC
                            focus:border-MainC
                            block w-full p-2.5 dark:bg-SecC
                            dark:border-MainC
                            dark:placeholder-red-800 dark:text-red-800" required={true}/>
                        </div>
                        <div className="flex justify-between">
                            <div className="flex items-start">
                                <div className="flex items-center h-5">
                                    {/* <input id="remember" aria-describedby="remember" type="checkbox" onChange={e => setSeguirConectado(e.target.value)} className="bg-SecC
                                    border  border-red-800 focus:ring-3 focus:ring-SecC
                                    h-4 w-4 rounded dark:bg-SecC
                                    dark:border-red-800 dark:focus:ring-SecC
                                    dark:ring-offset-red-800" required={false}/> */}
                                </div>
                                {/* <div className="text-medium ml-3">
                                <label htmlFor="remember" className="font-medium text-MainC
                                dark:text-MainC
                                " > Recordarme </label>
                                </div> */}
                            </div>
                            {/* <a href="/" className="text-medium text-MainC
                            hover:underline dark:text-MainC
                            ">Recuperar Credenciales</a> */}
                        </div>
                        <button type="submit" className="w-full text-Black bg-MainC
                        hover:bg-SecC
                        focus:ring-1 focus:ring-red-800 font-medium rounded-lg text-medium px-5 py-2.5 text-center dark:bg-MainC
                        dark:hover:bg-SecC
                        dark:focus:ring-red-800">Ingresa</button>
                        <div className="text-medium font-medium text-SecC
                        dark:text-SecC
                        ">
                            No estas Registrado ? <a href="/Registro" className="text-MainC
                            hover:underline dark:text-MainC
                            ">Crear una Cuenta</a>
                        </div>
                    </form>
                </div>
            </div>
        </div> 

        </div>
    )
}
Login.propTypes = {
    setToken: PropTypes.func.isRequired
    }




