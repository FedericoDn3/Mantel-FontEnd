import { data } from 'autoprefixer'
import React,{useState,useEffect} from 'react'
import { PayPalButton } from "react-paypal-button-v2"
import swal from 'sweetalert'


const ActualizarLogin = async (e) => {
    sessionStorage.setItem('Pago', e)

    swal({
        title:"Redirigiendo.",
        icon: "success",
        button: false,
        timer:500
    }).then(
        setTimeout(() => {
            window.location.href = 'http://localhost:3000/'
        }, 10000)

            // setTimeout(() => {    
    )

}

const Pagaos = () => {
    const paypalKey = ''//aqui la api key paypal
    return (
        <div>        <section className="py-8 leading-7  bg-white md:py-0">
        <div className="box-border px-4 mx-auto border-solid sm:px-6 md:px-6 lg:px-8 max-w-7xl">
            <div className="flex flex-col items-center leading-7 text-center text-gray-900 border-0 border-gray-200">
                <h2 className="box-border m-0 text-3xl font-bold leading-tight tracking-tight text-black border-solid sm:text-2xl md:text-3xl">
                    Precios, Simples y Transparentes.
                </h2>
                <p className="box-border mt-2 text-xl text-MainC border-solid sm:text-2xl">
                    Planes ajustados a tu bolsillo.
                </p>
            </div>
            <div className="grid grid-cols-1 gap-4 mt-4 leading-7 text-gray-900 border-0 border-gray-200 sm:mt-6 sm:gap-6 md:mt-8 md:gap-0 lg:grid-cols-3">
                {/* <!-- Price 1 --> */}
                <div className="relative z-10 flex flex-col items-center max-w-md p-4 mx-auto my-0 border border-solid rounded-lg lg:-mr-3 sm:my-0 sm:p-6 md:my-8 md:p-8">
                    <h3 className="m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl">
                        Por Semana
                    </h3>
                    <div className="flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200">
                        <p className="box-border m-0 text-6xl font-semibold leading-none border-solid">
                            $5
                        </p>
                        <p className="box-border m-0 border-solid">
                            / pe
                        </p>
                    </div>
                    <p className="mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200">
                        Ideal para probar sin compromisos.
                    </p>
                    <ul className="flex-1 p-0 mt-4 ml-5 leading-7 text-gray-900 border-0 border-gray-200">
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Series
                        </li>
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Películas
                        </li>
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Eventos en Vivo
                        </li>
                    </ul>
                    <PayPalButton
                        options={{vault: true,intent: "subscription" ,clientId: paypalKey}}
                        createSubscription={(data,actions) => {
                            return actions.subscription.create({
                            plan_id: 'P-1016115791397603HMLLSWJQ' //Aqui usar el plan id proporcionado por Paypal
                            });
                            }}
                            onApprove={(data, actions) => {
                                return actions.subscription.get().then(function(details) {
                                swal({
                                title: "Gracias Por Suscribirte a Mantel."+details.subscriber.name.given_name+ ", Disfruta de Todo Nuestro Contenido!",
                                icon: "success",
                                button: false,
                                timer:4000
                                });
                                const NuevaSusc = async () => { 
                                const settings = {
                                    method: 'POST',
                                    headers: {
                                    Accept: 'application/json',
                                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                                    },
                                    body: new URLSearchParams({
                                        id:sessionStorage.getItem("IdUsu"),
                                        pago:data.orderID,
                                        pagoSub:"semanal"
                                    })
                                }


                                fetch('http://localhost:8080/Java2022_war/Prueba/subscripcion',settings)
                                }
                                NuevaSusc().then(ActualizarLogin())
                                });
                            }}
                            style={{
                                size:  'responsive',
                                shape: 'pill',
                                color: 'gold',
                                layout: 'horizontal',
                                label: 'subscribe'
                            }}
                            />
                </div>
                {/* <!-- Price 2 --> */}
                <div className="relative z-20 flex flex-col items-center max-w-md p-4 mx-auto my-0 bg-white border-4 border-MainC border-solid rounded-lg sm:p-6 md:px-8 md:py-16">
                    <h3 className="m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl">
                        Mensual
                    </h3>
                    <div className="flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200">
                        <p className="box-border m-0 text-6xl font-semibold leading-none border-solid">
                            $15
                        </p>
                        <p className="box-border m-0 border-solid">
                            / mes
                        </p>
                    </div>
                    <p className="mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200">
                        Mira tu contenido todo el mes . cancela cuando quieras... o no.
                    </p>
                    <ul className="flex-1 p-0 mt-4 leading-7 text-gray-900 border-0 border-gray-200">
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Series
                        </li>
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Eventos
                        </li>
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Eventos en Vivo
                        </li>
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            La Opción mas elegida por el publico!
                        </li>
                    </ul>
                    <PayPalButton
                        options={{vault: true,intent: "subscription" ,clientId: paypalKey}}
                        createSubscription={(data,actions) => {
                            return actions.subscription.create({
                            plan_id: 'P-80W03250XU314921NMLLSYJY' //Aqui usar el plan id proporcionado por Paypal
                            });
                            }}
                            onApprove={(data, actions) => {
                                return actions.subscription.get().then(function(details) {
                                swal({
                                title: "Gracias Por Suscribirte a Mantel."+details.subscriber.name.given_name+ ", Disfruta de Todo Nuestro Contenido!",
                                icon: "success",
                                button: false,
                                timer:4000
                                });
                                const NuevaSusc = async () => {  
                                const settings = {
                                    method: 'POST',
                                    headers: {
                                    Accept: 'application/json',
                                    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                                    },
                                    body: new URLSearchParams({
                                        id:sessionStorage.getItem("IdUsu"),
                                        pago:data.orderID,
                                        pagoSub:"mensual"
                                    })
                                }


                                fetch('http://localhost:8080/Java2022_war/Prueba/subscripcion',settings)
                                }
                                NuevaSusc().then(ActualizarLogin())
                                });
                            }}
                            style={{
                                size:  'responsive',
                                shape: 'pill',
                                color: 'gold',
                                layout: 'horizontal',
                                label: 'subscribe'
                            }}
                            />
                </div>
                {/* <!-- Price 3 --> */}
                <div className="relative z-10 flex flex-col items-center max-w-md p-4 mx-auto my-0 border border-solid rounded-lg lg:-ml-3 sm:my-0 sm:p-6 md:my-8 md:p-8">
                    <h3 className="m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl">
                        Anual
                    </h3>
                    <div className="flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200">
                        <p className="box-border m-0 text-6xl font-semibold leading-none border-solid">
                            $150
                        </p>
                        <p className="box-border m-0 border-solid">
                            / anual
                        </p>
                    </div>
                    <p className="mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200">
                        Paga una vez disfruta todo el año!
                    </p>
                    <ul className="flex-1 p-0 mt-4 leading-7 text-gray-900 border-0 border-gray-200">
                        <li className="inline-flex items-center  w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Películas
                        </li>
                        <li className="inline-flex items-center  w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Series
                        </li>
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            Eventos en Vivo
                        </li>
                        <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                            <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                            </svg>
                            La opción Ahorro!
                        </li>
                    </ul>
                    <PayPalButton
                        options={{vault: true,intent: "subscription" ,clientId: paypalKey}}
                        createSubscription={(data,actions) => {
                            return actions.subscription.create({
                            plan_id: 'P-4BP60819KY848011YMLLSY5Y' //Aqui usar el plan id proporcionado por Paypal
                            });
                            }}
                        onApprove={(data, actions) => {
                            return actions.subscription.get().then(function(details) {
                            swal({
                            title: "Gracias Por Suscribirte a Mantel."+details.subscriber.name.given_name+ ", Disfruta de Todo Nuestro Contenido!",
                            icon: "success",
                            button: false,
                            timer:4000
                            });
                            const NuevaSusc = async () => {  
                            const settings = {
                                method: 'POST',
                                headers: {
                                Accept: 'application/json',
                                'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
                                },
                                body: new URLSearchParams({
                                    id:sessionStorage.getItem("IdUsu"),
                                    pago:data.orderID,
                                    pagoSub:"anual"
                                })
                            }


                            fetch('http://localhost:8080/Java2022_war/Prueba/subscripcion',settings)
                            }
                            NuevaSusc().then(ActualizarLogin())
                            });
                        }}
                        style={{
                            size:  'responsive',
                            shape: 'pill',
                            color: 'gold',
                            layout: 'horizontal',
                            label: 'subscribe'
                        }}
                    />
                </div>
            </div>
        </div>
    </section></div>
    )
}

export default Pagaos