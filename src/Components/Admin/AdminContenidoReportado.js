
import React ,{useState,useEffect} from 'react'


async function TraerContenido() {
  //Headers y body para el fetch.
  const settings = {
    method: 'POsT',
        headers: {
          Accept: 'application/json',
          'Access-Control-Allow-Methods': 'http://localhost:8080/Java2022_war/Prueba/getPendienteBloqueo'
          },
          body: new URLSearchParams({
          })
        }
  return fetch('http://localhost:8080/Java2022_war/Prueba/getPendienteBloqueo',settings).then(data => data.json())    
}

async function bloquearContenido(Id, booleano) {
  //Headers y body para el fetch.
  const settings = {
    method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
          },
          body: new URLSearchParams({
            IdContenido:Id,
            Aprobado:booleano
          })
        }
  return fetch('http://localhost:8080/Java2022_war/Prueba/bloquearContenido',settings).then(data => data)    
}
	
export default function AdminContenidoReportado() {
  const [contenidos = [] ,SetContenidos] = useState();
  const [mostrando = [] ,SetMostrando] = useState();
  const filtrar = (e) => {
    var m = []
    contenidos.forEach(x=> {
      if(x.titulo.includes(e)){
        m.push(x)
      }
    })
    SetMostrando(m)
  }
  const fijar= (data) => {
    SetContenidos(data)
    SetMostrando(data)
  }
  const  Veredicto = async (Id, booleano) => {
    bloquearContenido(Id, booleano)
    .then(d => {
      TraerContenido().then(data => fijar(data))
    })
  }
	useEffect(() => {
    TraerContenido().then(data => fijar(data))
	}, [])
  return(<>
	<div className="bg-white p-8 rounded-md w-full">
		<div className=" flex items-center justify-between pb-6">
			<div>
				<h2 className="text-gray-600 font-semibold">Contenido para aprobar</h2>
			</div>
		<div className="flex items-center justify-between">
			<div className="flex bg-gray-50 items-center p-2 rounded-md">
				<svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" viewBox="0 0 20 20"
					fill="currentColor">
					<path fillRule="evenodd"
						d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
						clipRule="evenodd" />
				</svg>
				  <input onKeyUp={e => filtrar(e.target.value)} className="bg-gray-50 outline-none ml-1 block " type="text" name="" id="" placeholder="Buscar..."/>
      </div>
    </div>
		</div>
		<div>
			<div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
				<div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
					<table className="min-w-full leading-normal">
						<thead>
							<tr>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Nombre
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Tipo
								</th>
                <th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Pais
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Detalles
								</th>
                <th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Aprobar
								</th>
							</tr>
						</thead>
						<tbody>
              {mostrando.map((C) => (<>
                  <tr key={C.id}>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <div className="flex items-center">
                        { C.imagen !== null &&  
                          <div className="flex-shrink-0 w-10 h-10">
                            <img className="w-full h-full rounded-full"
                              src={ C.imagen}
                              alt="" />
                          </div>
                        }
                          <div className="ml-3">
                            <p className="text-gray-900 whitespace-no-wrap">
                              {C.titulo}
                            </p>
                          </div>
                        </div>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {C.tipoContenido}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {C.duracion}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {C.idiomaOri}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {C.pais}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {C.director}
                      </p>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {/* <Link to={`/Mantel/${C.titulo}/${C.id}`}> */}
                        <span className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                          <span aria-hidden className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                          <span className="relative">Ver</span>
                        </span>
                      {/* </Link> */}
                    </td>
                    <td className="px-5 py-5 w-60 border-b border-gray-200 bg-white text-sm">
                      <span onClick={() => Veredicto(C.id,"si")} className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                        <span aria-hidden className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                        <span className="relative">Aprobar</span>
                      </span>
                      <span onClick={() => Veredicto(C.id,"no")} className="relative inline-block px-3 py-1 font-semibold text-red-900 leading-tight">
                        <span aria-hidden className="absolute inset-0 bg-red-200 opacity-50 rounded-full"></span>
                        <span className="relative">rechazar</span>
                      </span>
                    </td>
                  </tr>
                </>))
              }
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </>)
    
}