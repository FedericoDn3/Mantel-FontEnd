import { Result } from 'postcss';
import React ,{useState,useEffect} from 'react'
import { Link } from 'react-router-dom'
import { PayPalButton } from "react-paypal-button-v2"
import swal from 'sweetalert';

async function Traerdata() {
    //Headers y body para el fetch.
    const settings = {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
          },
        body: new URLSearchParams({
        })
    }
    return fetch('http://localhost:8080/Java2022_war/Prueba/listarPaperNoComprados',settings).then(data => data.json())    
}
      
export default function Reportes() {
  const [contenidos = [] ,SetContenidos] = useState();
  const [mostrando = [] ,SetMostrando] = useState();
  const filtrar = (e) => {
    var m = []
    contenidos.forEach(x=> {
      if(x.titulo.includes(e)){
        m.push(x)
      }
    })
    SetMostrando(m)
  }
	const C = [{
    "id": 1,
        "titulo": "Matrix",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":1,
        "Reportes":23,
        "puntaje": 3,
        "Vistas": 3123,
        "tipoContenido": "Peli",
        "Estado": "Activo",
      },{
        "id": 2,
        "titulo": "Duro de matar",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":0,
        "Reportes":23,
        "puntaje": 4,
        "Vistas": 12345,
        "tipoContenido": "Peli",
        "Estado": "Activo",
      },{
        "id": 3,
        "titulo": "Alien",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":0,
        "Reportes":12,
        "puntaje": 4,
        "Vistas": 1543261,
        "tipoContenido": "Peli",
        "Estado": "Activo",
      },{
        "id": 4,
        "titulo": "Terminator II",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":1,
        "Reportes":23,
        "puntaje": 5,
        "Vistas": 34261,
        "tipoContenido": "Peli",
        "Estado": "Activo",
      },{
        "id": 5,
        "titulo": "The truman show",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":0,
        "Reportes":31,
        "puntaje": 3,
        "Vistas": 435743,
        "tipoContenido": "Peli",
        "Estado": "Activo",
      },{
        "id": 6,
        "titulo": "Inframundo",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":0,
        "Reportes":23,
        "puntaje": 4,
        "Vistas": 456324,
        "tipoContenido": "Peli",
        "Estado": "Activo",
      },{
        "id": 7,
        "titulo": "El planeta del tesoro",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":1,
        "Reportes":23,
        "puntaje": 5,
        "Vistas": 4372452,
        "tipoContenido": "Peli",
        "Estado": "Inactivo",
      },{
        "id": 8,
        "titulo": "Avengers",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":0,
        "Reportes":234,
        "puntaje": 4,
        "Vistas": 3467452,
        "tipoContenido": "Peli",
        "Estado": "En Espera",
      },{
        "id": 9,
        "titulo": "Viernes 13",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":0,
        "Reportes":23,
        "puntaje": 3,
        "Vistas": 34722245,
        "tipoContenido": "Peli",
        "Estado": "Activo",
      },{
        "id": 10,
        "titulo": "El extraÃ±o mundo de Jack",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":1,
        "Reportes":23,
        "puntaje": 5,
        "Vistas": 245624,
        "tipoContenido": "Peli",
        "Estado": "Activo",
      },{
        "id": 11,
        "titulo": "Atlantis",
        "imagen": "7GFMZSG/1.jpg",
        "Destacado":0,
        "Reportes":23,
        "puntaje": 4,
        "Vistas": 23452,
        "tipoContenido": "Peli",
        "Estado": "Inactivo",
      },]
	useEffect(() => {
		let res = []
		if (res.length > 0)
			SetContenidos(res)
		else
			SetContenidos(C)
	}, [])
    return(<>
	<div className="bg-white p-8 rounded-md w-full">
		<div className=" flex items-center justify-between pb-6">
			<div>
				<h2 className="text-gray-600 font-semibold">Contenido</h2>
			</div>
		<div className="flex items-center justify-between">
			<div className="flex bg-gray-50 items-center p-2 rounded-md">
				<svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" viewBox="0 0 20 20"
					fill="currentColor">
					<path fillRule="evenodd"
						d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
						clipRule="evenodd" />
				</svg>
				  <input onKeyUp={e => filtrar(e.target.value)} className="bg-gray-50 outline-none ml-1 block " type="text" name="" id="" placeholder="Buscar..."/>
      </div>
    </div>
		</div>
		<div>
			<div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
				<div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
					<table className="min-w-full leading-normal">
						<thead>
							<tr>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Nombre
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Tipo
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Calificación
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Status
								</th>
							</tr>
						</thead>
						<tbody>
              {mostrando.length > 0 &&
                mostrando.map((C) => (<>
                  <tr key={C.id}>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <div className="flex items-center">
                          <div className="flex-shrink-0 w-10 h-10">
                            <img className="w-full h-full rounded-full"
                              src={ C.imagen}
                              alt="" />
                          </div>
                          <div className="ml-3">
                            <p className="text-gray-900 whitespace-no-wrap">
                              {C.titulo}
                            </p>
                          </div>
                        </div>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {C.tipoContenido}
                      </p>
                    </td>
                    <td className="px-5 py-5 text-justify border-b border-gray-200 bg-white text-sm">
                      <div className=" flex gap-2" type="button" data-modal-toggle="default-modal">
                        {C.puntaje > 0 &&
                        <span type="button" data-modal-toggle="default-modal" className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                          <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                        {C.puntaje > 1 &&
                        <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                        {C.puntaje > 2 &&
                        <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                        {C.puntaje > 3 &&
                        <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                        {C.puntaje > 4 &&
                        <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                      </div>
                    </td>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      {C.Estado === "Activo" &&
                      <><span className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                          <span aria-hidden className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                          <span className="relative">Disponible</span>
                        </span></>
                      }
                      {C.Estado === "Inactivo" &&
                      <Link to={`/Mantel/${C.titulo}/${C.id}`}>
                        <span className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                          <span aria-hidden className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                          <span className="relative">Ver</span>
                        </span>
                      </Link>
                      }
                      {C.Estado === "En Espera" &&
                      <span className="relative inline-block px-3 py-1 font-semibold text-blue-900 leading-tight">
                        <span aria-hidden className="absolute inset-0 bg-blue-200 opacity-50 rounded-full"></span>
                          <span className="relative">En Espera</span>
                      </span>
                      }
                    </td>
                  </tr>
                </>))
              }

              { mostrando.length === 0 &&
                contenidos.map((C) => (<>
                <tr key={C.id}>
                  <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <div className="flex items-center">
                        <div className="flex-shrink-0 w-10 h-10">
                          <img className="w-full h-full rounded-full"
                            src={ C.imagen}
                            alt="" />
                        </div>
                        <div className="ml-3">
                          <p className="text-gray-900 whitespace-no-wrap">
                            {C.titulo}
                          </p>
                        </div>
                      </div>
                  </td>
                  <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <p className="text-gray-900 whitespace-no-wrap">
                      {C.tipoContenido}
                    </p>
                  </td>
                  <td className="px-5 py-5 text-justify border-b border-gray-200 bg-white text-sm">
                    <div className=" flex gap-2" type="button" data-modal-toggle="default-modal">
                      {C.puntaje > 0 &&
                      <span type="button" data-modal-toggle="default-modal" className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                      </span>}
                      {C.puntaje > 1 &&
                      <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                          <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                      </span>}
                      {C.puntaje > 2 &&
                      <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                          <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                      </span>}
                      {C.puntaje > 3 &&
                      <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                          <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                      </span>}
                      {C.puntaje > 4 &&
                      <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                          <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                      </span>}
                    </div>
                  </td>
                  <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    {C.Estado === "Activo" &&
                    <><span className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                    <span aria-hidden className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                    <span className="relative">Disponible</span>
                  </span></>
                    }
                    {C.Estado === "Inactivo" &&
                    <Link to={`/Mantel/${C.titulo}/${C.id}`}>
                      <span className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                        <span aria-hidden className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
                        <span className="relative">Ver</span>
                      </span>
                    </Link>
                    }
                    {C.Estado === "En Espera" &&
                    <span className="relative inline-block px-3 py-1 font-semibold text-blue-900 leading-tight">
                      <span aria-hidden className="absolute inset-0 bg-blue-200 opacity-50 rounded-full"></span>
                        <span className="relative">En Espera</span>
                    </span>
                    }
                  </td>
                </tr>
                </>))
              }
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </>)
    
}