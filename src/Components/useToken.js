import { useState } from 'react';

export default function useToken() {
  const getToken = () => {
    const tokenString = sessionStorage.getItem('Peps');
    const userToken = JSON.parse(tokenString);
    return userToken
  };

  const [token, setToken] = useState(getToken());

  const saveToken = userToken => {
    sessionStorage.setItem('Peps', JSON.stringify(userToken))//si quiero solo el token poner .accesstoken
    //sessionStorage.setItem('ftoken', JSON.stringify(userToken.access_token))
    //setToken(userToken.access_token);
    setToken(userToken.tipoUsuario);
  }

  return {
    setToken: saveToken,
    token
  }
}