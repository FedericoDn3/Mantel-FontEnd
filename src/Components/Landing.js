import React from 'react'
import { Link } from 'react-router-dom'

const Landing = () => {
  return (
    <div>
        {/* // <!-- Section 1 --> */}
        <section className="px-2 py-20 bg-white md:px-0"> 
            <div className="container items-center max-w-6xl px-8 mx-auto xl:px-5">
                <div className="flex flex-wrap items-center sm:-mx-3">
                <div className="w-full md:w-1/2 md:px-3">
                    <div className="w-full pb-6 space-y-6 sm:max-w-md lg:max-w-lg md:space-y-4 lg:space-y-8 xl:space-y-9 sm:pr-5 lg:pr-0 md:pb-0">
                    <h1 className="text-4xl font-extrabold tracking-tight text-gray-900 sm:text-5xl md:text-4xl lg:text-5xl xl:text-6xl">
                        <span className="block xl:inline">Gran contenido</span>
                        <span className="block text-MainC xl:inline">&nbsp; Para Maravillosas Personas!</span>
                    </h1>
                    <p className="mx-auto text-base text-gray-500 sm:max-w-md lg:text-xl md:max-w-3xl">En Mantel trabajamos con los mejores proveedores para traer la selección mas variada y emocionante de títulos para tu entretenimiento.</p>
                    <div className="relative flex flex-col sm:flex-row sm:space-x-4">
                        <a href="/RegistroProobedor" className="flex items-center w-full px-6 py-3 mb-3 text-lg text-white bg-MainC rounded-md sm:mb-0 hover:bg-sky-800  sm:w-auto">
                            Quiero ser Colaborador
                        <svg xmlns="http://www.w3.org/2000/svg" className="w-5 h-5 ml-1" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>
                        </a>
                    </div>
                    </div>
                </div>
                <div className="w-full md:w-1/2">
                    <div className="w-full h-auto overflow-hidden rounded-md shadow-xl sm:rounded-xl">
                        <img alt="Manteleando" src="https://img.freepik.com/foto-gratis/pareja-viendo-serivicio-streaming-juntos-casa-interiores_23-2148977261.jpg"/> 
                    </div>
                </div>
                </div>
            </div>
        </section>

        {/* //<!-- Section 2 --> */}
        <section className="py-20 bg-white md:px-0">
            <div className="container max-w-6xl mx-auto">
                <h2 className="text-4xl font-bold tracking-tight text-center">Te Ofrecemos</h2>
                <p className="mt-2 text-lg text-center text-MainC">Algunas de nuestras Características.</p>
                <div className="grid grid-cols-4 gap-8 mt-10 sm:grid-cols-8 lg:grid-cols-12 sm:px-8 xl:px-0">

                    <div className="relative flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 overflow-hidden bg-gray-100 sm:rounded-xl">
                        <div className="p-3 text-white bg-MainC rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-8 h-8 " viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M14 3v4a1 1 0 0 0 1 1h4"></path><path d="M5 8v-3a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2h-5"></path><circle cx="6" cy="14" r="3"></circle><path d="M4.5 17l-1.5 5l3 -1.5l3 1.5l-1.5 -5"></path></svg>
                        </div>
                        <h4 className="text-xl font-medium text-gray-700">Películas</h4>
                        <p className="text-base text-center text-gray-500">La mejor selección que encontraras en la Web.</p>
                    </div>

                    <div className="flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl">
                        <div className="p-3 text-white bg-MainC rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-8 h-8 " viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M18 8a3 3 0 0 1 0 6"></path><path d="M10 8v11a1 1 0 0 1 -1 1h-1a1 1 0 0 1 -1 -1v-5"></path><path d="M12 8h0l4.524 -3.77a0.9 .9 0 0 1 1.476 .692v12.156a0.9 .9 0 0 1 -1.476 .692l-4.524 -3.77h-8a1 1 0 0 1 -1 -1v-4a1 1 0 0 1 1 -1h8"></path></svg>
                        </div>
                        <h4 className="text-xl font-medium text-gray-700">Series</h4>
                        <p className="text-base text-center text-gray-500">Todas tus series favoritas , de ayer , hoy y siempre.</p>
                    </div>

                    <div className="flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl">
                        <div className="p-3 text-white bg-MainC rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-8 h-8 " viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><polyline points="12 3 20 7.5 20 16.5 12 21 4 16.5 4 7.5 12 3"></polyline><line x1="12" y1="12" x2="20" y2="7.5"></line><line x1="12" y1="12" x2="12" y2="21"></line><line x1="12" y1="12" x2="4" y2="7.5"></line><line x1="16" y1="5.25" x2="8" y2="9.75"></line></svg>
                        </div>
                        <h4 className="text-xl font-medium text-gray-700">Eventos en Vivo</h4>
                        <p className="text-base text-center text-gray-500">En Mantel siempre tenemos los eventos mas populares en demanda.</p>
                    </div>

                    <div className="flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl">
                        <div className="p-3 text-white bg-MainC rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-8 h-8 " viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M8 9l3 3l-3 3"></path><line x1="13" y1="15" x2="16" y2="15"></line><rect x="3" y="4" width="18" height="16" rx="2"></rect></svg>
                        </div>
                        <h4 className="text-xl font-medium text-gray-700">Estrenos</h4>
                        <p className="text-base text-center text-gray-500">Estrenos en simultaneo con las salas de cine , desde la comodidad de tu Sofa</p>
                    </div>

                    <div className="flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl">
                        <div className="p-3 text-white bg-MainC rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-8 h-8 " viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><line x1="9.5" y1="11" x2="9.51" y2="11"></line><line x1="14.5" y1="11" x2="14.51" y2="11"></line><path d="M9.5 15a3.5 3.5 0 0 0 5 0"></path><path d="M7 5h1v-2h8v2h1a3 3 0 0 1 3 3v9a3 3 0 0 1 -3 3v1h-10v-1a3 3 0 0 1 -3 -3v-9a3 3 0 0 1 3 -3"></path></svg>
                        </div>
                        <h4 className="text-xl font-medium text-gray-700">Eventos Deportivos</h4>
                        <p className="text-base text-center text-gray-500">La Champions ,El Superbowl , Los Clásicos mas importantes , todo en la mejor calidad! .</p>
                    </div>

                    <div className="flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl">
                        <div className="p-3 text-white bg-MainC rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" className="w-8 h-8 " viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><line x1="15" y1="5" x2="15" y2="7"></line><line x1="15" y1="11" x2="15" y2="13"></line><line x1="15" y1="17" x2="15" y2="19"></line><path d="M5 5h14a2 2 0 0 1 2 2v3a2 2 0 0 0 0 4v3a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2v-3a2 2 0 0 0 0 -4v-3a2 2 0 0 1 2 -2"></path></svg>
                        </div>
                        <h4 className="text-xl font-medium text-gray-700">Eventos Musicales</h4>
                        <p className="text-base text-center text-gray-500">Conciertos a un paso de ti , con el mejor sonido y una calidad de video inigualable.</p>
                    </div>

                </div>
            </div>
        </section>

        {/* //<!-- Section 3 --> */}
        <section className="py-8 leading-7  bg-white md:py-0">
            <div className="box-border px-4 mx-auto border-solid sm:px-6 md:px-6 lg:px-8 max-w-7xl">
                <div className="flex flex-col items-center leading-7 text-center text-gray-900 border-0 border-gray-200">
                    <h2 className="box-border m-0 text-3xl font-bold leading-tight tracking-tight text-black border-solid sm:text-2xl md:text-3xl">
                        Precios, Simples y Transparentes.
                    </h2>
                    <p className="box-border mt-2 text-xl text-MainC border-solid sm:text-2xl">
                        Planes ajustados a tu bolsillo.
                    </p>
                </div>
                <div className="grid grid-cols-1 gap-4 mt-4 leading-7 text-gray-900 border-0 border-gray-200 sm:mt-6 sm:gap-6 md:mt-8 md:gap-0 lg:grid-cols-3">
                    {/* <!-- Price 1 --> */}
                    <div className="relative z-10 flex flex-col items-center max-w-md p-4 mx-auto my-0 border border-solid rounded-lg lg:-mr-3 sm:my-0 sm:p-6 md:my-8 md:p-8">
                        <h3 className="m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl">
                            Por Semana
                        </h3>
                        <div className="flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200">
                            <p className="box-border m-0 text-6xl font-semibold leading-none border-solid">
                                $5
                            </p>
                            <p className="box-border m-0 border-solid">
                                / pe
                            </p>
                        </div>
                        <p className="mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200">
                            Ideal para probar sin compromisos.
                        </p>
                        <ul className="flex-1 p-0 mt-4 ml-5 leading-7 text-gray-900 border-0 border-gray-200">
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Series
                            </li>
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Películas
                            </li>
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Eventos en Vivo
                            </li>
                        </ul>
                        <Link to="/Registro">
                        <button component={Link} to="/Register" className="inline-flex justify-center w-full px-4 py-3 mt-8 font-sans text-sm leading-none text-center text-MainC no-underline bg-transparent border border-MainC rounded-md cursor-pointer hover:bg-blue-700 hover:border-blue-700 hover:text-white focus-within:bg-blue-700 focus-within:border-blue-700 focus-within:text-white sm:text-base md:text-lg">
                            Me Interesa
                        </button>
                        </Link>
                    </div>
                    {/* <!-- Price 2 --> */}
                    <div className="relative z-20 flex flex-col items-center max-w-md p-4 mx-auto my-0 bg-white border-4 border-MainC border-solid rounded-lg sm:p-6 md:px-8 md:py-16">
                        <h3 className="m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl">
                            Mensual
                        </h3>
                        <div className="flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200">
                            <p className="box-border m-0 text-6xl font-semibold leading-none border-solid">
                                $130
                            </p>
                            <p className="box-border m-0 border-solid">
                                / mes
                            </p>
                        </div>
                        <p className="mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200">
                            Mira tu contenido todo el mes . cancela cuando quieras... o no.
                        </p>
                        <ul className="flex-1 p-0 mt-4 leading-7 text-gray-900 border-0 border-gray-200">
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Series
                            </li>
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Eventos
                            </li>
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Eventos en Vivo
                            </li>
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                La Opción mas elegida por el publico!
                            </li>
                        </ul>
                        <Link to="/Registro">
                        <button component={Link} to="/Register" className="inline-flex justify-center w-full px-4 py-3 mt-8 font-sans text-sm leading-none text-center text-white no-underline bg-MainC border rounded-md cursor-pointer hover:bg-blue-700 hover:border-blue-700 hover:text-white focus-within:bg-blue-700 focus-within:border-blue-700 focus-within:text-white sm:text-base md:text-lg">
                            Me Interesa
                        </button>
                        </Link>
                    </div>
                    {/* <!-- Price 3 --> */}
                    <div className="relative z-10 flex flex-col items-center max-w-md p-4 mx-auto my-0 border border-solid rounded-lg lg:-ml-3 sm:my-0 sm:p-6 md:my-8 md:p-8">
                        <h3 className="m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl">
                            Anual
                        </h3>
                        <div className="flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200">
                            <p className="box-border m-0 text-6xl font-semibold leading-none border-solid">
                                $1500
                            </p>
                            <p className="box-border m-0 border-solid">
                                / anual
                            </p>
                        </div>
                        <p className="mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200">
                            Paga una vez disfruta todo el año!
                        </p>
                        <ul className="flex-1 p-0 mt-4 leading-7 text-gray-900 border-0 border-gray-200">
                            <li className="inline-flex items-center  w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Películas
                            </li>
                            <li className="inline-flex items-center  w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Series
                            </li>
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                Eventos en Vivo
                            </li>
                            <li className="inline-flex items-center w-full mb-2 ml-5 font-semibold text-left border-solid">
                                <svg className="w-5 h-5 mr-2 font-semibold leading-7 text-MainC sm:h-5 sm:w-5 md:h-6 md:w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7"></path>
                                </svg>
                                La opción Ahorro!
                            </li>
                        </ul>
                        <Link to="/Registro">
                        <button component={Link} to="/Register" className="inline-flex justify-center w-full px-4 py-3 mt-8 font-sans text-sm leading-none text-center text-MainC no-underline bg-transparent border border-MainC rounded-md cursor-pointer hover:bg-blue-700 hover:border-blue-700 hover:text-white focus-within:bg-blue-700 focus-within:border-blue-700 focus-within:text-white sm:text-base md:text-lg">
                            Me Interesa
                        </button>
                        </Link>
                    </div>
                </div>
            </div>
        </section>

        {/* //<!-- Section 4 --> */}
        <section className="flex items-center justify-center py-16 bg-gray-100 min-w-screen">
            <div className="max-w-6xl px-12 mx-auto bg-gray-100 md:px-16 xl:px-10">
                <div className="flex flex-col items-center lg:flex-row">
                    <div className="flex flex-col items-start justify-center w-full h-full pr-8 mb-10 lg:mb-0 lg:w-1/2">
                        <p className="mb-2 text-base font-medium tracking-tight text-MainC uppercase">Todo mundo quiere tener un Mantel en su casa.</p>
                        <h2 className="text-4xl font-extrabold leading-10 tracking-tight text-gray-900 sm:text-5xl sm:leading-none md:text-6xl lg:text-5xl xl:text-6xl">Testimonios</h2>
                        <p className="my-6 text-lg text-gray-600">Pero no nos crea solo a nosotros,he aquí una lista de testimonios que demuestran que la gente ama lo que hacemos.</p>
                    </div>
                    <div className="w-full lg:w-1/2">
                        <blockquote className="flex items-center justify-between w-full col-span-1 p-6 bg-white rounded-lg shadow">
                            <div className="flex flex-col pr-8">
                                <div className="relative pl-12">
                                    <svg className="absolute left-0 w-10 h-10 text-MainC fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125">
                                        <path d="M30.7 42c0 6.1 12.6 7 12.6 22 0 11-7.9 19.2-18.9 19.2C12.7 83.1 5 72.6 5 61.5c0-19.2 18-44.6 29.2-44.6 2.8 0 7.9 2 7.9 5.4S30.7 31.6 30.7 42zM82.4 42c0 6.1 12.6 7 12.6 22 0 11-7.9 19.2-18.9 19.2-11.8 0-19.5-10.5-19.5-21.6 0-19.2 18-44.6 29.2-44.6 2.8 0 7.9 2 7.9 5.4S82.4 31.6 82.4 42z"></path>
                                    </svg>
                                    <p className="mt-2 text-sm text-gray-600 sm:text-base lg:text-sm xl:text-base">Le veo mas futuro que el Dogecoin.</p>
                                </div>

                                <h3 className="pl-12 mt-3 text-sm font-medium leading-5 text-gray-800 truncate sm:text-base lg:text-base">
                                    Elon para los amigos
                                    <span className="mt-1 text-sm leading-5 text-gray-500 truncate">- CEO Genérico</span>
                                </h3>
                            </div>
                            
                            {/* <img className="flex-shrink-0 w-20 h-20 bg-gray-300 rounded-full xl:w-24 xl:h-24" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=4&amp;w=256&amp;h=256&amp;q=60" alt=""/> */}
                            <img className="flex-shrink-0 w-20 h-20 bg-gray-300 rounded-full xl:w-24 xl:h-24" src="https://cnnespanol.cnn.com/wp-content/uploads/2022/05/220504175105-03-elon-musk-twitter-antitrust-full-169.jpg?quality=100&strip=info" alt=""/>
                        </blockquote>
                        <blockquote className="flex items-center justify-between w-full col-span-1 p-6 mt-4 bg-white rounded-lg shadow">
                            <div className="flex flex-col pr-10">
                                <div className="relative pl-12">
                                    <svg className="absolute left-0 w-10 h-10 text-MainC fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125">
                                        <path d="M30.7 42c0 6.1 12.6 7 12.6 22 0 11-7.9 19.2-18.9 19.2C12.7 83.1 5 72.6 5 61.5c0-19.2 18-44.6 29.2-44.6 2.8 0 7.9 2 7.9 5.4S30.7 31.6 30.7 42zM82.4 42c0 6.1 12.6 7 12.6 22 0 11-7.9 19.2-18.9 19.2-11.8 0-19.5-10.5-19.5-21.6 0-19.2 18-44.6 29.2-44.6 2.8 0 7.9 2 7.9 5.4S82.4 31.6 82.4 42z"></path>
                                    </svg>
                                    <p className="mt-2 text-sm text-gray-600 sm:text-base lg:text-sm xl:text-base">Sin Mantel no podría ver mis monitas chinas.</p>
                                </div>
                                <h3 className="pl-12 mt-3 text-sm font-medium leading-5 text-gray-800 truncate sm:text-base lg:text-base">
                                    Stiven Spielberg
                                    <span className="mt-1 text-sm leading-5 text-gray-500 truncate">- Cineasta Indie</span>
                                </h3>
                                <p className="mt-1 text-sm leading-5 text-gray-500 truncate"></p>
                            </div>
                            <img className="flex-shrink-0 w-24 h-24 bg-gray-300 rounded-full" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHVS2ee6usybjD-SvuXSvnWU2E3qf7-5g5YOTbUK71OHqtPVTY" alt=""/>
                        </blockquote>
                        <blockquote className="flex items-center justify-between w-full col-span-1 p-6 mt-4 bg-white rounded-lg shadow">
                            <div className="flex flex-col pr-10">
                                <div className="relative pl-12">
                                    <svg className="absolute left-0 w-10 h-10 text-MainC fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125">
                                        <path d="M30.7 42c0 6.1 12.6 7 12.6 22 0 11-7.9 19.2-18.9 19.2C12.7 83.1 5 72.6 5 61.5c0-19.2 18-44.6 29.2-44.6 2.8 0 7.9 2 7.9 5.4S30.7 31.6 30.7 42zM82.4 42c0 6.1 12.6 7 12.6 22 0 11-7.9 19.2-18.9 19.2-11.8 0-19.5-10.5-19.5-21.6 0-19.2 18-44.6 29.2-44.6 2.8 0 7.9 2 7.9 5.4S82.4 31.6 82.4 42z"></path>
                                    </svg>
                                    <p className="mt-2 text-sm text-gray-600 sm:text-base lg:text-sm xl:text-base">Perfecto para un día de Picnic.</p>
                                </div>

                                <h3 className="pl-12 mt-3 text-sm font-medium leading-5 text-gray-800 truncate sm:text-base lg:text-base">
                                    Sebastian Morales
                                    <span className="mt-1 text-sm leading-5 text-gray-500 truncate">- Sujeto Genérico</span>
                                </h3>
                                <p className="mt-1 text-sm leading-5 text-gray-500 truncate"></p>
                            </div>
                            <img className="flex-shrink-0 w-24 h-24 bg-gray-300 rounded-full" src="https://i.ibb.co/8P89gxG/unknown.png" alt=""/>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>


        {/* //<!-- Section 5 --> */}
        <section className="bg-white">
            <div className="max-w-screen-xl px-4 py-12 mx-auto space-y-8 overflow-hidden sm:px-6 lg:px-8">
                <nav className="flex flex-wrap justify-center -mx-5 -my-2">
                    <div className="px-5 py-2">
                        <a href="/" className="text-base leading-6 text-gray-500 hover:text-gray-900">
                            Sobre Nosotros.
                        </a>
                    </div>
                    <div className="px-5 py-2">
                        <a href="/" className="text-base leading-6 text-gray-500 hover:text-gray-900">
                            Proveedores.
                        </a>
                    </div>
                    <div className="px-5 py-2">
                        <a href="/" className="text-base leading-6 text-gray-500 hover:text-gray-900">
                            Precios.
                        </a>
                    </div>
                    <div className="px-5 py-2">
                        <a href="/" className="text-base leading-6 text-gray-500 hover:text-gray-900">
                            Contacto.
                        </a>
                    </div>
                    <div className="px-5 py-2">
                        <a href="/" className="text-base leading-6 text-gray-500 hover:text-gray-900">
                            Términos y Condiciones.
                        </a>
                    </div>
                </nav>
                <div className="flex justify-center mt-8 space-x-6">
                    <a href="/" className="text-MainC hover:text-sky-800">
                        <span className="sr-only">Facebook</span>
                        <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 24 24">
                            <path fillRule="evenodd" d="M22 12c0-5.523-4.477-10-10-10S2 6.477 2 12c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V12h2.54V9.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V12h2.773l-.443 2.89h-2.33v6.988C18.343 21.128 22 16.991 22 12z" clipRule="evenodd"></path>
                        </svg>
                    </a>
                    <a href="/" className="text-MainC hover:text-text-sky-800">
                        <span className="sr-only">Instagram</span>
                        <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 24 24">
                            <path fillRule="evenodd" d="M12.315 2c2.43 0 2.784.013 3.808.06 1.064.049 1.791.218 2.427.465a4.902 4.902 0 011.772 1.153 4.902 4.902 0 011.153 1.772c.247.636.416 1.363.465 2.427.048 1.067.06 1.407.06 4.123v.08c0 2.643-.012 2.987-.06 4.043-.049 1.064-.218 1.791-.465 2.427a4.902 4.902 0 01-1.153 1.772 4.902 4.902 0 01-1.772 1.153c-.636.247-1.363.416-2.427.465-1.067.048-1.407.06-4.123.06h-.08c-2.643 0-2.987-.012-4.043-.06-1.064-.049-1.791-.218-2.427-.465a4.902 4.902 0 01-1.772-1.153 4.902 4.902 0 01-1.153-1.772c-.247-.636-.416-1.363-.465-2.427-.047-1.024-.06-1.379-.06-3.808v-.63c0-2.43.013-2.784.06-3.808.049-1.064.218-1.791.465-2.427a4.902 4.902 0 011.153-1.772A4.902 4.902 0 015.45 2.525c.636-.247 1.363-.416 2.427-.465C8.901 2.013 9.256 2 11.685 2h.63zm-.081 1.802h-.468c-2.456 0-2.784.011-3.807.058-.975.045-1.504.207-1.857.344-.467.182-.8.398-1.15.748-.35.35-.566.683-.748 1.15-.137.353-.3.882-.344 1.857-.047 1.023-.058 1.351-.058 3.807v.468c0 2.456.011 2.784.058 3.807.045.975.207 1.504.344 1.857.182.466.399.8.748 1.15.35.35.683.566 1.15.748.353.137.882.3 1.857.344 1.054.048 1.37.058 4.041.058h.08c2.597 0 2.917-.01 3.96-.058.976-.045 1.505-.207 1.858-.344.466-.182.8-.398 1.15-.748.35-.35.566-.683.748-1.15.137-.353.3-.882.344-1.857.048-1.055.058-1.37.058-4.041v-.08c0-2.597-.01-2.917-.058-3.96-.045-.976-.207-1.505-.344-1.858a3.097 3.097 0 00-.748-1.15 3.098 3.098 0 00-1.15-.748c-.353-.137-.882-.3-1.857-.344-1.023-.047-1.351-.058-3.807-.058zM12 6.865a5.135 5.135 0 110 10.27 5.135 5.135 0 010-10.27zm0 1.802a3.333 3.333 0 100 6.666 3.333 3.333 0 000-6.666zm5.338-3.205a1.2 1.2 0 110 2.4 1.2 1.2 0 010-2.4z" clipRule="evenodd"></path>
                        </svg>
                    </a>
                    <a href="/" className="text-MainC hover:text-text-sky-800">
                        <span className="sr-only">Twitter</span>
                        <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 24 24">
                            <path d="M8.29 20.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0022 5.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.072 4.072 0 012.8 9.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 012 18.407a11.616 11.616 0 006.29 1.84"></path>
                        </svg>
                    </a>
                </div>
                <p className="mt-8 text-base leading-6 text-center text-gray-400">
                    © 2022 Mantel, Inc. Derechos Reservados.
                </p>
            </div>
        </section>  


    </div>
  )
}

export default Landing