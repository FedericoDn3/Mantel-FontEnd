import React, { useState } from 'react';
import Logo from '../Images/MantelL.png'
import swal from 'sweetalert';

const Register = () => {

    async function RegistrarUsu(credentials) {
        const settings = {
            method: 'POST',
            headers: {
            Accept: 'application/json',
            'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
            },
            body: new URLSearchParams({
                mail:credentials.id,
                password:credentials.password,
                nombre:credentials.nombre,
                apellido:credentials.apellido,
                pais:credentials.lugar
              })
          }
        fetch('http://localhost:8080/Java2022_war/Prueba/Registro',settings);


    
        swal({
            title: credentials.nombre + "\u00a0Mantel te da la Bienvenida!",
            icon: "success",
            button: false,
            timer:3000
        });
          setTimeout(() => {    
            window.location.href = 'http://localhost:3000/'}, 3000)
        }   
    
            const [id, setId] = useState();
            const [password, setPassword] = useState();
            const [nombre, setNombre] = useState();
            const [apellido, setApellido] = useState();
            const [lugar, setLugar] = useState();
        
            const handleSubmit = async (e) => {
                e.preventDefault();
                RegistrarUsu({
                    id,
                    password,
                    nombre,
                    apellido,
                    lugar
            })
        }



  return (
    

            <section className="w-full bg-white">

                <div className="mx-auto max-w-7xl">
                    <div className="flex flex-col lg:flex-row">
                        <div className="relative w-full bg-cover lg:w-6/12 xl:w-7/12 bg-gradient-to-r from-white via-white to-gray-100">
                            <div className="relative flex flex-col items-center justify-center w-full h-full px-10 my-20 lg:px-16 lg:my-0">
                                <div className="flex flex-col items-start space-y-8 tracking-tight lg:max-w-3xl">
                                    <div className="relative">
                                        <img src={Logo} alt="Logo" className='flex place-items-center align-middle' style={{height:'15vh'}} />
                                        <h2 className="text-5xl font-bold text-gray-900 xl:text-6xl">¡Estás a un solo paso!</h2>
                                    </div>
                                    <p className="text-2xl text-MainC">En Mantel nos aseguramos de darte el mejor entretenimiento al precio más justo del mercado!</p>
                                </div>
                            </div>
                        </div>

                        <div className="w-full bg-white lg:w-6/12 xl:w-5/12">
                            <form onSubmit={handleSubmit}>
                                <div className="flex flex-col items-start justify-start w-full h-full p-10 lg:p-16 xl:p-24">
                                    <h4 className="w-full text-3xl font-bold">Regístrate ya!</h4>
                                    <div className="relative w-full mt-10 space-y-8">
                                        <div className="relative">
                                            <label className="font-medium text-gray-900">Nombre</label>
                                            <input type="text" onChange={e => setNombre(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu nombre." required={true}/>
                                        </div>
                                        <div className="relative">
                                            <label className="font-medium text-gray-900">Apellido</label>
                                            <input type="text" onChange={e => setApellido(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu apellido" required={true}/>
                                        </div>
                                        <div className="relative">
                                            <label className="font-medium text-gray-900">País</label>
                                            <input type="text" onChange={e => setLugar(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu País" required={true}/>
                                        </div>
                                        <div className="relative">
                                            <label className="font-medium text-gray-900">Email</label>
                                            <input type="email" onChange={e => setId(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="TuEmail@Algo.com" required={true}/>
                                        </div>
                                        <div className="relative">
                                            <label className="font-medium text-gray-900">Contraseña</label>
                                            <input type="password" onChange={e => setPassword(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="*********" required={true}/>
                                        </div>
                                        <div className="relative">
                                            <button type="submit" className="inline-block w-full px-5 py-4 text-lg font-medium text-center text-white transition duration-200 bg-MainC rounded-lg hover:bg-blue-700 ease">Listo!</button>
                                            {/* <a href="#_" className="inline-block w-full px-5 py-4 mt-3 text-lg font-bold text-center text-gray-900 transition duration-200 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 ease">Google</a> */}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </section>

  )
}

export default Register