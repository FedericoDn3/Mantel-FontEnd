import React, { useState } from 'react';
import swal from 'sweetalert';

const Contenido = () => {

    async function AltaContenido(credentials) {
        const settings = {
            method: 'POST',
            headers: {
            Accept: 'application/json',
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: new URLSearchParams({
                titulo:credentials.titulo,
                ubicacion:credentials.ubicacion,
                tipo:credentials.tipo,
                imagen:credentials.imagen,
              })
          }


    
        swal({
            title: credentials.nombre + "\u00a0Mantel te da la Bienvenida!",
            icon: "success",
            button: false,
            timer:3000
        });
        }   
    
            const [categoria, setCategoria] = useState();
           
        
            const handleSubmit = async (e) => {
                e.preventDefault();
                AltaContenido({
                    categoria
                    
            })

            if(categoria=='Película'){
                window.location.href = 'http://localhost:3000/AltaPelicula';
            }
            if(categoria=='Serie'){
                window.location.href = 'http://localhost:3000/AltaSerie';
            }
            if(categoria=='Evento'){
                window.location.href = 'http://localhost:3000/AltaEvento';
            }
            

        }



  return (
    

            <section className="w-full bg-white">

                <div className="w-full">
                    <div className="flex flex-col lg:flex-row ">
                        
                      
                        <div className="w-full h-full bg-white lg:w-6/12 xl:w-6/12 ml-auto mr-auto">
                            <form onSubmit={handleSubmit}>
                                <div className="flex flex-col items-start justify-start w-full h-full p-10 lg:p-16 xl:p-24 ">
                                    

                                    <div className="relative w-full mt-10 space-y-8 content-center" >
                                    <div className="relative">
                                            <select type="text" onChange={e => setCategoria(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí sus categorías" >
                                                <option>Elige un tipo de contenido</option>
                                                <option>Película</option>
                                                <option>Serie</option>
                                                <option>Evento</option>
                                                
                                            </select>
                                        </div>
                                        

                                        <div className="relative flex-col">
                                            <button type="submit" className="inline-block w-full px-5 py-4 text-lg font-medium text-center text-white transition duration-200 bg-MainC rounded-lg hover:bg-blue-700 ease">Siguiente</button>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
              

            </section>

  )
}

export default Contenido