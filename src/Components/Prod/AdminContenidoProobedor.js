import { Result } from 'postcss';
import {useState,useEffect} from 'react'

async function masVistaProductor(id) {
  //Headers y body para el fetch.
	const settings = {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
			},
		body: new URLSearchParams({
			IdProductor:id
		})
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/masVistaProductor',settings).then(data => data.json())    
}

async function DestacarContenido(id) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
        },
      body: new URLSearchParams({
        IdProductor:id
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/listarContenidosProductor',settings).then(data => data.json())    
}

export default function AdminContenidoProobedor({Cont}) {
const [contenido = [],SetContenido] = useState();
  const [mostrando = [] ,SetMostrando] = useState();
  const iduser = sessionStorage.getItem("IdUsu")
  const filtrar = (e) => {
    var m = []
    contenido.forEach(x=> {
      if(x.titulo.includes(e)){
        m.push(x)
      }
    })
    SetMostrando(m)
  }
  const  Destacar = async (Id) => {
    DestacarContenido(Id)
    .then(data => {
		masVistaProductor(iduser).then(data => SetContenido(data))
    })
  }
  useEffect(() => {
    masVistaProductor(iduser).then(data => SetContenido(data))
  }, [])
    return(<>
<div className="bg-white p-8 rounded-md w-full">
	<div className=" flex items-center justify-between pb-6">
		<div>
			<h2 className="text-gray-600 font-semibold">Contenido</h2>
		</div>
		<div className="flex items-center justify-between">
			<div className="flex bg-gray-50 items-center p-2 rounded-md">
				<svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" viewBox="0 0 20 20"
					fill="currentColor">
					<path fillRule="evenodd"
						d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
						clipRule="evenodd" />
				</svg>
				    <input onKeyUp={e => filtrar(e.target.value)} className="bg-gray-50 outline-none ml-1 block " type="text" name="" id="" placeholder="Buscar..."/>
            </div>
        </div>
		</div>
		<div>
			<div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
				<div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
					<table className="min-w-full leading-normal">
						<thead>
							<tr>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Nombre
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Tipo
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Vistas
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Calificación
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Destacado
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Status
								</th>
							</tr>
						</thead>
						<tbody>
						{mostrando.map((C) => (<>
							<tr key={C.id}>
								<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
									<div className="flex items-center">
											<div className="ml-3">
												<p className="text-gray-900 whitespace-no-wrap">
													{C.titulo}
												</p>
											</div>
										</div>
								</td>
								<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
									<p className="text-gray-900 whitespace-no-wrap">
										{C.tipoContenido}
									</p>
								</td>
                <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
									<p className="text-gray-900 whitespace-no-wrap">
									{C.vistas}
									</p>
								</td>
								<td className="px-5 py-5 text-justify border-b border-gray-200 bg-white text-sm">
									<p className="text-gray-900 whitespace-no-wrap">{C.Nota}</p>
								</td>
								<td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
									{C.Destacado === 1 &&
									<svg onClick={() => Destacar(C.id)} xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-orange-600" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
										<path strokeLinecap="round" strokeLinejoin="round" d="M17.657 18.657A8 8 0 016.343 7.343S7 9 9 10c0-2 .5-5 2.986-7C14 5 16.09 5.777 17.656 7.343A7.975 7.975 0 0120 13a7.975 7.975 0 01-2.343 5.657z" />
										<path strokeLinecap="round" strokeLinejoin="round" d="M9.879 16.121A3 3 0 1012.015 11L11 14H9c0 .768.293 1.536.879 2.121z" />
									</svg>}
									{C.Destacado === 0 &&
									<svg onClick={() => Destacar(C.id)} xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
										<path strokeLinecap="round" strokeLinejoin="round" d="M17.657 18.657A8 8 0 016.343 7.343S7 9 9 10c0-2 .5-5 2.986-7C14 5 16.09 5.777 17.656 7.343A7.975 7.975 0 0120 13a7.975 7.975 0 01-2.343 5.657z" />
										<path strokeLinecap="round" strokeLinejoin="round" d="M9.879 16.121A3 3 0 1012.015 11L11 14H9c0 .768.293 1.536.879 2.121z" />
									</svg>}
								</td>
								{/* <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
									{C.Estado === "Activo" &&
									<span
                  className="relative inline-block px-3 py-1 font-semibold text-green-900 leading-tight">
                  <span aria-hidden
                      className="absolute inset-0 bg-green-200 opacity-50 rounded-full"></span>
									    <span className="relative">Activo</span>
									</span>
									}
									{C.Estado === "Inactivo" &&
									<span className="relative inline-block px-3 py-1 font-semibold text-red-900 leading-tight">
										<span aria-hidden className="absolute inset-0 bg-red-200 opacity-50 rounded-full"></span>
									    <span className="relative">Inactivo</span>
									</span>
									}
									{C.Estado === "En Espera" &&
									<span className="relative inline-block px-3 py-1 font-semibold text-orange-900 leading-tight">
										<span aria-hidden className="absolute inset-0 bg-orange-200 opacity-50 rounded-full"></span>
									    <span className="relative">En Espera</span>
									</span>
									}
								</td> */}
							</tr>
						</>))}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </>)
    
}