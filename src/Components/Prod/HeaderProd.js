/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import {ChartBarIcon,MenuIcon,XIcon,DocumentAddIcon} from '@heroicons/react/outline'
import { ChevronDownIcon } from '@heroicons/react/solid'
import Logo from '../../Images/MantelL.png'
import swal from 'sweetalert';


const resources = [
  // {
  //   name: 'Agregar Pelicula',
  //   description: 'Agregar Pelicula - Sujeto a Revision de Administradores.',
  //   href: '/AltaPelicula',
  //   icon: DocumentAddIcon,
  // },
  // {
  //   name: 'Agregar Serie',
  //   description: 'Agregar Serie - Sujeto a Revision de Administradores.',
  //   href: '/AltaSerie',
  //   icon: DocumentAddIcon,
  // },
  // {
  //   name: 'Agregar Evento',
  //   description: 'Agregar Evento - Sujeto a Revision de Administradores.',
  //   href: '/AltaEvento',
  //   icon: DocumentAddIcon,
  // },
  {
    name: 'Agregar Contenido',
    description: 'Agregar Contenido - Sujeto a Revision de Administradores.',
    href: '/AltaContenido',
    icon: DocumentAddIcon,
  },
  {
    name: 'Reportes',
    description: 'Reportes de Contenidos.',
    href: '/',
    icon: ChartBarIcon,
  }
]


function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

const Logout = () => {
  //alert("Hasta la Proxima")
  
  swal({
      title: "Hasta la próxima , 'Mantel' te desea un lindo día.",
      icon: "info",
      button: false,
      timer:3000
  });
  setTimeout(() => {    
      sessionStorage.clear()
      window.location.reload(false);}, 500);
  }


export default function HeaderProd() {
  return (

    <div>
    <Popover className="relative bg-CineCalidad">
      <div className="max-w-7xl mx-auto px-4 sm:px-6">
        <div className="flex justify-between items-center border-b-2 border-gray-800 py-6 md:justify-start md:space-x-10">
          <div className="flex justify-start lg:w-fit  lg:flex border-SecC px-2 border-2">
            <a href="/" className='flex'>
              {/* Logo */}
              <span className="sr-only">Mantelito</span>
              <img src={Logo} alt="Logo" style={{height:'5vh' }} />
              <p className='font-Cc text-4xl  text-MainC' >&nbsp; Mantel &nbsp;</p>
            </a>
          </div>
          {/* Menu Hamburguesa */}
          <div className="-mr-2 -my-2 md:hidden">

            <Popover.Button className="bg-MainC rounded-md p-2 inline-flex items-center justify-center text-black hover:bg-SecC focus:outline-none focus:ring-2 focus:ring-inset focus:ring-SecC ">
              <span className="sr-only">Open menu</span>
              <MenuIcon className="h-6 w-6" aria-hidden="true" />
            </Popover.Button>
          </div>
          <Popover.Group as="nav" className="hidden md:flex space-x-10">


              {/* 1a Botonera */}
            <Popover className="relative">
              {({ open }) => (
                <>
                  <Popover.Button
                    className={classNames(
                      open ? 'text-SecC' : 'text-MainC',
                      'group bg-CineCalidad rounded-md inline-flex items-center text-base font-medium hover:text-SecC focus:outline-none focus:ring-2 focus:ring-offset-1 focus:ring-SecC'

                      )}
                  >
                    <span> Panel Control</span>
                    <ChevronDownIcon
                      className={classNames(
                        open ? 'text-SecC' : 'text-MainC',
                        'ml-2 h-5 w-5 group-hover:text-SecC'
                      )}
                      aria-hidden="true"
                    />
                  </Popover.Button>

                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-200"
                    enterFrom="opacity-0 translate-y-1"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in duration-150"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 translate-y-1"
                  >
                    <Popover.Panel className="absolute z-10 left-1/2 transform -translate-x-1/2 mt-3 px-2 w-screen max-w-md sm:px-0">
                      <div className="rounded-lg shadow-lg ring-2 ring-SecC ring-opacity-1 overflow-hidden">
                        <div className="relative grid gap-6 bg-CineCalidad px-5 py-6 sm:gap-8 sm:p-8">
                          {resources.map((item) => (
                            <a
                              key={item.name}
                              href={item.href}
                              className="-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50"
                            >
                              <item.icon className="flex-shrink-0 h-6 w-6 text-MainC" aria-hidden="true" />
                              <div className="ml-4">
                                <p className="text-base font-medium text-SecC">{item.name}</p>
                                <p className="mt-1 text-sm text-yellow-400">{item.description}</p>
                              </div>
                            </a>
                          ))}
                        </div>
                        {/* Sección Extra Con Posts o Noticias
                        <div className="px-5 py-5 bg-gray-50 sm:px-8 sm:py-8">
                          <div>
                            <h3 className="text-sm tracking-wide font-medium text-gray-500 uppercase">Recent Posts</h3>
                            <ul role="list" className="mt-4 space-y-4">
                              {recentPosts.map((post) => (
                                <li key={post.id} className="text-base truncate">
                                  <a href={post.href} className="font-medium text-gray-900 hover:text-gray-700">
                                    {post.name}
                                  </a>
                                </li>
                              ))}
                            </ul>
                          </div>
                           <div className="mt-5 text-sm">
                            <a href="/" className="font-medium text-indigo-600 hover:text-indigo-500">
                              {' '}
                              View all posts <span aria-hidden="true">&rarr;</span>
                            </a>
                          </div> 
                        </div>*/}
                      </div>
                    </Popover.Panel>
                  </Transition>
                </>
              )}
            </Popover>
          </Popover.Group>
          <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
         {/* Aquí poner Botón Logout*/}
            <button onClick={()=>Logout()} className="bg-MainC self-center hover:bg-blue-800 text-black  py-2 px-4 rounded">Salir</button>
          </div>
        </div>
      </div>
        {/* Modo Celular */}
      <Transition
        as={Fragment}
        enter="duration-200 ease-out"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="duration-100 ease-in"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Popover.Panel focus className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden">
          <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-black divide-y-2 divide-MainC">            
            <div className="pt-5 pb-6 px-5 bg-black">
              <div className="flex items-center justify-between">
                <div className='flex'>
                  <img src={Logo} alt="Logo" style={{height:'5vh' }} />
                  <p className='font-Cc text-xl  text-MainC' >&nbsp; Mantel &nbsp;</p>
                </div>
                <div className="-mr-2">
                  <Popover.Button className="bg-black rounded-md p-2 inline-flex items-center justify-center text-MainC hover:text-gray-500 hover:bg-MainC focus:outline-none focus:ring-2 focus:ring-inset focus:ring-MainC">
                    <span className="sr-only">Close menu</span>
                    <XIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
              <div className="mt-6">
                <nav className="grid gap-y-8">
                  {resources.map((item) => (
                    <a
                      key={item.name}
                      href={item.href}
                      className="-m-3 p-3 flex items-center rounded-md hover:bg-gray-50"
                    >
                      <item.icon className="flex-shrink-0 h-6 w-6 text-MainC " aria-hidden="true" />
                      <span className="ml-3 text-base font-medium text-SecC">{item.name}</span>
                    </a>
                  ))}
                </nav>
              </div>
            </div>
            <div className="py-6 flex justify-center px-5 space-y-6">
              <div className=" bg-black flex justify-center gap-x-8">
                <button onClick={()=>Logout()} className="bg-MainC hover:bg-SecC text-black  py-2 px-4 rounded">Salir</button>
              </div>
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
    </div>
  )
}