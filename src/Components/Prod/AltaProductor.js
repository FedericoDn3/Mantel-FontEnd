import React, { useState } from 'react';
import Logo from '../../Images/MantelL.png';
import swal from 'sweetalert';


const OK = async (e) => {
    swal({
        title: "Has agregado una nueva película!",
        icon: "success",
        button: false,
        timer:3000
    });
}

async function Send(credentials) {
    const settings = {
        method: 'POST',
        headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
        },
        body: new URLSearchParams({
            nombre:credentials.nombre,
            password:credentials.password,
            mail:credentials.mail,
            pais:credentials.pais,
            telefono:credentials.telefono,
            web:credentials.web,
            logo:credentials.logo,
          })
      }
      return fetch('http://localhost:8080/Java2022_war/Prueba/Postulacion',settings).then(data => data.json()).then(OK())    
  }

const AltaProductor = () => {
    
    const [nombre, setNombre] = useState();
    const [password, setPassword] = useState();
    const [mail, setMail] = useState();
    const [pais, setPais] = useState();
    const [telefono, setTelefono] = useState();
    const [web, setWeb] = useState();
    const [logo, setLogo] = useState();

    
    const handleSubmit = async (e) => {
        e.preventDefault();
        Send({
            nombre,
            password,
            mail,
            pais,
            telefono,
            web,
            logo
       })
    }



  return (
    <section className="w-full bg-white">
        <div className="mx-auto max-w-7xl">
            <div className="flex flex-col lg:flex-row">
                <div className="relative w-full bg-cover lg:w-6/12 xl:w-7/12 bg-gradient-to-r from-white via-white to-gray-100">
                    <div className="relative flex flex-col items-center justify-center w-full h-full px-10 my-20 lg:px-16 lg:my-0">
                        <div className="flex flex-col items-start space-y-8 tracking-tight lg:max-w-3xl">
                            <div className="relative">
                                <img src={Logo} alt="Logo" className='flex place-items-center align-middle' style={{height:'15vh'}} />
                                <h2 className="text-5xl font-bold text-gray-900 xl:text-6xl">¡Estás por postularte como productor de Mantel!</h2>
                            </div>
                            <p className="text-2xl text-MainC">En Mantel nos aseguramos de darte el mejor entretenimiento al precio más justo del mercado!</p>
                        </div>
                    </div>
                </div>

                <div className="w-full bg-white lg:w-6/12 xl:w-5/12">
                    <form onSubmit={handleSubmit}>
                        <div className="flex flex-col items-start justify-start w-full h-full p-10 lg:p-16 xl:p-24">
                            <h4 className="w-full text-3xl font-bold">Postulate para productor ya!</h4>
                            <div className="relative w-full mt-10 space-y-8">
                                <div className="relative">
                                    <label className="font-medium text-gray-900">Nombre</label>
                                    <input type="text" onChange={e => setNombre(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu nombre." required={true}/>
                                </div>
                                <div className="relative">
                                    <label className="font-medium text-gray-900">Contraseña</label>
                                    <input type="password" onChange={e => setPassword(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu password" required={true}/>
                                </div>
                                <div className="relative">
                                    <label className="font-medium text-gray-900">País</label>
                                    <input type="text" onChange={e => setPais(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu país" required={true}/>
                                </div>
                                <div className="relative">
                                    <label className="font-medium text-gray-900">Email</label>
                                    <input type="email" onChange={e => setMail(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="TuEmail@Algo.com" required={true}/>
                                </div>
                                
                                <div className="relative">
                                    <label className="font-medium text-gray-900">Teléfono</label>
                                    <input type="text" onChange={e => setTelefono(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu teléfono" required={true}/>
                                </div>
                                <div className="relative">
                                    <label className="font-medium text-gray-900">Web</label>
                                    <input type="text" onChange={e => setWeb(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu web" required={true}/>
                                </div>
                                <div className="relative">
                                    <label className="font-medium text-gray-900">Logo</label>
                                    <input type="text" onChange={e => setLogo(e.target.value)} className="block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50" placeholder="Aquí tu logo" required={true}/>
                                </div>
                                <div className="relative">
                                    <button type="submit" className="inline-block w-full px-5 py-4 text-lg font-medium text-center text-white transition duration-200 bg-MainC rounded-lg hover:bg-blue-700 ease">Listo!</button>
                                    {/* <a href="/" className="inline-block w-full px-5 py-4 mt-3 text-lg font-bold text-center text-gray-900 transition duration-200 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 ease">Google</a> */}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

  )
}

export default AltaProductor