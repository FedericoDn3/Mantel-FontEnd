import React ,{useState,useEffect} from 'react'
import { Link } from 'react-router-dom'


async function GetFavoritos(id) {
    const settings = {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
          },
        body: new URLSearchParams({
          id:id
        })
    }
    return fetch('http://localhost:8080/Java2022_war/Prueba/listarFavoritos',settings).then(data => data.json())    
}

async function GEtComprados(id) {
    const settings = {
    method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
          },
          body: new URLSearchParams({
            id:id
          })
        }
        return fetch('http://localhost:8080/Java2022_war/Prueba/listarPaperComprados',settings).then(data => data.json())    
      }
      
export default function PPVListado() {
    const [favoritos = [] ,SetFavoritos] = useState();
    const [comprados = [] ,SetComprados] = useState();
    const filtrarF = (e) => {
    var m = []
    favoritos.forEach(x=> {
        if(x.titulo.includes(e)){
        m.push(x)
        }
    })
    SetFavoritos(m)
    }
    const filtrarC = (e) => {
    var m = []
    comprados.forEach(x=> {
        if(x.titulo.includes(e)){
        m.push(x)
        }
    })
    SetComprados(m)
    }
    const iduser = sessionStorage.getItem("IdUsu")
    useEffect(() => {
        GetFavoritos(iduser).then(data => SetFavoritos(data))
        GEtComprados(iduser).then(data => SetComprados(data))
    }, [])
    return(<>
	<div className="bg-white p-8 rounded-md w-full">
		<div className=" flex items-center justify-between pb-6">
			<div>
				<h2 className="text-gray-600 font-semibold">Favoritos</h2>
			</div>
            <div className="flex items-center justify-between">
                <div className="flex bg-gray-50 items-center p-2 rounded-md">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" viewBox="0 0 20 20"
                        fill="currentColor">
                        <path fillRule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            clipRule="evenodd" />
                    </svg>
                    <input onKeyUp={e => filtrarF(e.target.value)} className="bg-gray-50 outline-none ml-1 block " type="text" name="" id="" placeholder="Buscar..."/>
                </div>
            </div>
		</div>
		<div>
			<div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
				<div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
					<table className="min-w-full leading-normal">
						<thead>
							<tr>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Nombre
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Tipo
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Calificación
								</th>
							</tr>
						</thead>
						<tbody>
                        { favoritos.map((C) => (<>
                            <tr key={C.id}>
                                <Link to={`/Peli/${C.id}`}>
                                    <div className="flex items-center">
                                        <div className="flex-shrink-0 w-10 h-10">
                                            <img className="w-full h-full rounded-full"
                                            src={ C.imagen}
                                            alt="" />
                                        </div>
                                        <div className="ml-3">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {C.titulo}
                                            </p>
                                        </div>
                                    </div>
                                </Link>
                                <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                <p className="text-gray-900 whitespace-no-wrap">
                                    {C.tipoContenido}
                                </p>
                                </td>
                                <td className="px-5 py-5 text-justify border-b border-gray-200 bg-white text-sm">
                                <div className=" flex gap-2" type="button" data-modal-toggle="default-modal">
                                    {C.puntaje > 0 &&
                                    <span type="button" data-modal-toggle="default-modal" className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                    <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                    {C.puntaje > 1 &&
                                    <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                    {C.puntaje > 2 &&
                                    <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                    {C.puntaje > 3 &&
                                    <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                    {C.puntaje > 4 &&
                                    <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                </div>
                                </td>
                            </tr>
                            </>))}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    <div className="bg-white p-8 rounded-md w-full">
		<div className=" flex items-center justify-between pb-6">
			<div>
				<h2 className="text-gray-600 font-semibold">Comprados</h2>
			</div>
            <div className="flex items-center justify-between">
                <div className="flex bg-gray-50 items-center p-2 rounded-md">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" viewBox="0 0 20 20"
                        fill="currentColor">
                        <path fillRule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            clipRule="evenodd" />
                    </svg>
                    <input onKeyUp={e => filtrarC(e.target.value)} className="bg-gray-50 outline-none ml-1 block " type="text" name="" id="" placeholder="Buscar..."/>
                </div>
            </div>
		</div>
		<div>
			<div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
				<div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
					<table className="min-w-full leading-normal">
						<thead>
							<tr>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Nombre
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Tipo
								</th>
								<th
									className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
									Calificación
								</th>
							</tr>
						</thead>
						<tbody>
                        { comprados.map((C) => (<>
                            <tr key={C.id}>
                                <Link to={`/Peli/${C.id}`}>
                                    <div className="flex items-center">
                                        <div className="flex-shrink-0 w-10 h-10">
                                            <img className="w-full h-full rounded-full"
                                            src={ C.imagen}
                                            alt="" />
                                        </div>
                                        <div className="ml-3">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {C.titulo}
                                            </p>
                                        </div>
                                    </div>
                                </Link>
                                <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                <p className="text-gray-900 whitespace-no-wrap">
                                    {C.tipoContenido}
                                </p>
                                </td>
                                <td className="px-5 py-5 text-justify border-b border-gray-200 bg-white text-sm">
                                <div className=" flex gap-2" type="button" data-modal-toggle="default-modal">
                                    {C.puntaje > 0 &&
                                    <span type="button" data-modal-toggle="default-modal" className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                    <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                    {C.puntaje > 1 &&
                                    <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                    {C.puntaje > 2 &&
                                    <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                    {C.puntaje > 3 &&
                                    <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                    {C.puntaje > 4 &&
                                    <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                                    <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                                    </svg>
                                    </span>}
                                </div>
                                </td>
                            </tr>
                            </>))}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </>)
    
}