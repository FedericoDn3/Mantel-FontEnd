import React,{useState,useEffect} from 'react'
import Slider from "react-slick";
import MPowePass from "../../Images/MPowePass.png"



const Accion = []

async function GetContenido(id) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        id:id
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/listarContenidoCarrusel',settings).then(data => data.json())    
}
async function ultimaVista(id) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        IdCliente:id
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/ultimaVista',settings).then(data => data.json())    
}

async function listarFavoritos(id) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        id:id
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/listarFavoritos',settings).then(data => data.json())    
}


const LandingCli = () => {
  const [content=[],setContent] = useState([])
  const [favoritos=[],setfavoritos] = useState([])
  const [ultimosVistos=[],setultimosVistos] = useState([])
  const iduser = sessionStorage.getItem("IdUsu")

  var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 6,
      slidesToScroll: 4
    };


  useEffect(() => {
    GetContenido(iduser).then(data => setContent(data))
    listarFavoritos(iduser).then(data => setfavoritos(data))
    ultimaVista(iduser).then(data => setultimosVistos(data))
  },[])

    // Llamar Content



    






     return (
        <>
      <div className="flex bg-black">
        <img src={MPowePass} alt="PowerPassMantel" className="m-2 ml-auto mr-auto " style={{ height: '40vh' }} />
      </div>
      <div>
      { favoritos.length > 0 && <>
        <h3 className="text-white flex justify-left text-3xl font-semibold" alt="Disfruta:">Favoritos</h3>
          <Slider {...settings}>
            <link
              rel="stylesheet"
              type="text/css"
              charSet="UTF-8"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
            <link
              rel="stylesheet"
              type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
            { favoritos.length > 0 &&
              favoritos.map(
              (C) => (
              <div className="py-3 justify-left bg-slate-500">
                <a href={`Peli/${C.id}`}>
                  <img src={C.imagen} alt="imagen" border="1" />
                </a>
              </div>
            ))}
          </Slider></>}
      { ultimosVistos.length > 0 && <>
        <h3 className="text-white flex justify-left text-3xl font-semibold" alt="Disfruta:">Estabas viendo:</h3>
          <Slider {...settings}>
            <link
              rel="stylesheet"
              type="text/css"
              charSet="UTF-8"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
            <link
              rel="stylesheet"
              type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
            { ultimosVistos.length > 0 &&
              ultimosVistos.map(
              (C) => (
              <div className="py-3 justify-left bg-slate-500">
                <a href={`Peli/${C.id}`}>
                  <img src={C.imagen} alt="imagen" border="1" />
                </a>
              </div>
            ))}
          </Slider></>}
        { content.map((fila) => (
          <>
          { fila.categoria!=="CienciaFiccion" && fila.contenidos.length > 0 && <h3 className="text-white flex justify-left text-3xl font-semibold" alt="Disfruta:">{fila.categoria}</h3>}
          { fila.categoria!=="CienciaFiccion" && <Slider {...settings}>
            <link
              rel="stylesheet"
              type="text/css"
              charSet="UTF-8"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
            <link
              rel="stylesheet"
              type="text/css"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
            { fila.contenidos.length > 0 &&
              fila.contenidos.map(
              (C) => (
              <div className="py-3 justify-left bg-slate-500">
                <a href={`Peli/${C.id}`}>
                  <img src={C.imagen} alt="imagen" border="1" />
                </a>
              </div>
            ))}
          </Slider>}</>
        ))}
    </div></>
    )

}

export default LandingCli
