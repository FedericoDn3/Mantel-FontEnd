import { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import {MenuIcon,XIcon,CurrencyDollarIcon,FilmIcon,PlayIcon,UserIcon} from '@heroicons/react/outline'
import Logo from '../../Images/MantelL.png'
import swal from 'sweetalert';
import {useState,useEffect} from 'react'

const solutions = [
  {
    name: 'Películas',
    description: '.',
    href: '/Peliculas',
    icon: FilmIcon,
  },
  {
    name: 'Series',
    description: '.',
    href: '/Series',
    icon: PlayIcon,
  },
  {
    name: 'Eventos',
    description: '.',
    href: '/Eventos',
    icon: CurrencyDollarIcon,
  },
  {
    name: 'Perfil',
    description: '.',
    href: '/Perfil',
    icon: UserIcon,
  }
]


const Logout = () => {
  //alert("Hasta la Proxima")
  
  swal({
      title: "Hasta la próxima , 'Mantel' te desea un lindo día.",
      icon: "info",
      button: false,
      timer:3000
  });
  setTimeout(() => {    
      sessionStorage.clear()
      window.location.reload(false);}, 500);
  }

  const handelKeydown = (e) => {
    if (e.key === 'Enter')
      window.location.href = `http://localhost:3000/Buscar/${e.target.value}`
  }


export default function HeaderCli() {
  return (
    <div>
    <Popover className="relative bg-CineCalidad">
      <div className="max-w-7xl mx-auto px-4 sm:px-6">
        <div className="flex justify-between items-center border-b-2 border-gray-800 py-6 md:justify-start md:space-x-10">
          <div className="flex justify-start lg:w-fit  lg:flex border-SecC px-2 border-2">
            <a href="/" className='flex'>
              {/* Logo */}
              <span className="sr-only">Mantelito</span>
              <img src={Logo} alt="Logo" style={{height:'5vh' }} />
              <p className='font-Cc text-4xl  text-MainC' >&nbsp; Mantel &nbsp;</p>
            </a>
          </div>
          {/* Menu Hamburguesa */}
          <div className="-mr-2 -my-2 md:hidden">

            <Popover.Button className="bg-MainC rounded-md p-2 inline-flex items-center justify-center text-black hover:bg-SecC focus:outline-none focus:ring-2 focus:ring-inset focus:ring-SecC ">
              <span className="sr-only">Open menu</span>
              <MenuIcon className="h-6 w-6" aria-hidden="true" />
            </Popover.Button>
          </div>
          <Popover.Group as="nav" className="hidden md:flex space-x-10">
          <Popover className="relative">
            {({ open }) => (
              <>
                <Transition
                  as={Fragment}
                  enter="transition ease-out duration-200"
                  enterFrom="opacity-0 translate-y-1"
                  enterTo="opacity-100 translate-y-0"
                  leave="transition ease-in duration-150"
                  leaveFrom="opacity-100 translate-y-0"
                  leaveTo="opacity-0 translate-y-1"
                >
                  <Popover.Panel className="absolute z-10 -ml-4 mt-3 transform px-2 w-screen max-w-md sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2">
                    <div className="rounded-lg shadow-lg ring-2 ring-SecC  ring-opacity-1 overflow-hidden">
                      <div className="relative grid gap-6 bg-CineCalidad px-5 py-6 sm:gap-8 sm:p-8">
                        {solutions.map((item) => (
                          <a
                            key={item.name}
                            href={item.href}
                            className="-m-3 p-3 flex items-start rounded-lg hover:bg-white"
                          >
                            <item.icon className="flex-shrink-0 h-6 w-6 text-MainC" aria-hidden="true" />
                            <div className="ml-4">
                              <p className="text-base font-medium text-SecC">{item.name}</p>
                              <p className="mt-1 text-sm text-yellow-400">{item.description}</p>
                            </div>
                          </a>
                        ))}
                      </div>
                      {/* Posible Agregación para soporte
                      <div className="px-5 py-5 bg-gray-50 space-y-6 sm:flex sm:space-y-0 sm:space-x-10 sm:px-8">
                        {callsToAction.map((item) => (
                          <div key={item.name} className="flow-root">
                            <a
                              href={item.href}
                              className="-m-3 p-3 flex items-center rounded-md text-base font-medium text-gray-900 hover:bg-gray-100"
                            >
                              <item.icon className="flex-shrink-0 h-6 w-6 text-gray-400" aria-hidden="true" />
                              <span className="ml-3">{item.name}</span>
                            </a>
                          </div>
                        ))}
                      </div>*/}
                    </div>
                  </Popover.Panel>
                </Transition>
              </>
            )}
          </Popover>
            {/* Divisiones Simples */}
          <a href="/Peliculas" className="text-base font-medium text-MainC hover:text-SecC">
            Películas
          </a>
          <a href={`/BuscarSeries/`} className="text-base font-medium text-MainC hover:text-SecC">
            Series
          </a>
          <a href="/Eventos" className="text-base font-medium text-MainC hover:text-SecC">
            Eventos
          </a>
          <a href="/Perfil" className="text-base font-medium text-MainC hover:text-SecC">
            Perfil
          </a>
          </Popover.Group>
          <div className="hidden md:flex ml-auto mr-auto bg-gray-700 items-center p-2 rounded-md">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" viewBox="0 0 20 20"
              fill="currentColor">
              <path fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd" />
            </svg>
              {/* <input onKeyDown={e => buscar(e.target.value)} className="bg-gray-500 outline-none ml-1 block " type="text" name="" id="" placeholder="buscar..."/> */}
              <input onKeyDown={e => handelKeydown(e)} className="bg-gray-500 outline-none ml-1 block " type="text" name="" id="" placeholder="buscar..."/>
          </div>
          <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
         {/* Aquí poner Botón Logout*/}
            <button onClick={()=>Logout()} className="bg-MainC self-center hover:bg-blue-800 text-black  py-2 px-4 rounded">Salir</button>
          </div>
        </div>
      </div>
        {/* Modo Celular */}
      <Transition
        as={Fragment}
        enter="duration-200 ease-out"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="duration-100 ease-in"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Popover.Panel focus className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden">
          <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 z-50 bg-black divide-y-2 divide-MainC">            
            <div className="pt-5 pb-6 px-5 z-50 bg-black">
              <div className="flex items-center justify-between">
                <div className='flex'>
                  <img src={Logo} alt="Logo" style={{height:'5vh' }} />
                  <p className='font-Cc text-xl  text-MainC' >&nbsp; Mantel &nbsp;</p>
                </div>
                <div className="-mr-2">
                  <Popover.Button className="bg-black rounded-md p-2 inline-flex items-center justify-center text-MainC hover:text-gray-500 hover:bg-MainC focus:outline-none focus:ring-2 focus:ring-inset focus:ring-MainC">
                    <span className="sr-only">Close menu</span>
                    <XIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
              <div className="mt-6">
                <nav className="grid gap-y-8">
                  {solutions.map((item) => (
                    <a
                      key={item.name}
                      href={item.href}
                      className="-m-3 p-3 flex items-center rounded-md hover:bg-gray-50"
                    >
                      <item.icon className="flex-shrink-0 h-6 w-6 text-MainC " aria-hidden="true" />
                      <span className="ml-3 text-base font-medium text-SecC">{item.name}</span>
                    </a>
                  ))}
                </nav>
              </div>
              <div className="flex mt-5 bg-gray-800 mr-5 items-center p-2 rounded-md">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-gray-400" viewBox="0 0 20 20"
                  fill="currentColor">
                  <path fillRule="evenodd"
                    d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                    clipRule="evenodd" />
                </svg>
                  <input onKeyDown={e => handelKeydown(e)} className="bg-gray-500 outline-none ml-1 block " type="text" name="" id="" placeholder="buscar..."/>
              </div>
            </div>
            <div className="py-6 flex justify-center px-5 space-y-6">
              <div className=" bg-black flex justify-center gap-x-8">
                <button onClick={()=>Logout()} className="bg-MainC hover:bg-SecC text-black  py-2 px-4 rounded">Salir</button>
              </div>
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
    </div>
  )
}
