import { Result } from 'postcss';
import {useState,useEffect} from 'react'

async function Comentar(C) {
    const settings = {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
          },
        body: new URLSearchParams(C)
    }
    return fetch('http://localhost:8080/Java2022_war/Prueba/comentarContenido',settings).then(data => data.json())    
  }
async function MarcarSpoiler(idComentario, idCliente) {
    const settings = {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
          },
        body: new URLSearchParams({
            idComentario:idComentario,
            idCliente:idCliente
        })
    }
    return fetch('http://localhost:8080/Java2022_war/Prueba/reportarComentario',settings).then(data => data.json())    
  }

export default function ComentariosItem({comentarios, IdCliente, IdContenido}) {
    const iduser = sessionStorage.getItem("IdUsu")
    const [comentario, setComentario] = useState();
    const [padre, setPadre] = useState();
    const handleSubmit = async (e) => {
        e.preventDefault();
        const C = {
            idContenido:IdContenido,
            idPadre:padre,
            idCliente:IdCliente,
            texto:comentario
        }
        Comentar(C)
    }
    const actualizarData = (Comentario, IDPadre) => {
        setComentario(Comentario)
        setPadre(IDPadre)
    }
    if (comentarios && comentarios.length !== 0) {
        return (
            <>
                {comentarios.map((Com) => (<>
                    <div key={Com.Id} className="bg-white rounded border border-slate-400 my-3">
                    { !Com.reportado  && 
                        <div className="py-2 px-3 ">
                            <div className="border-b border-slate-600 py-2">
                            <p className="text-sm text-blue-400 ">
                                {Com.NombreAutor}
                            </p>
                            <p className="text-sm mt-1 text-gray-400">
                                {Com.Comentario}
                            </p>
                            </div>
                            <form onSubmit={handleSubmit} className="bg-white shadow rounded-lg mb-6 p-4">
                                <textarea onChange={e => actualizarData(e.target.value, Com.Id)} name="message" placeholder="Comenta algo" className="w-full rounded-lg p-2 text-sm bg-gray-100 border border-transparent appearance-none rounded-tg placeholder-gray-400"></textarea>
                                <footer  className="flex justify-between mt-2">
                                    <div className="flex gap-2">
                                        <span onClick={() => MarcarSpoiler(Com.Id,iduser)} className="flex items-center transition ease-out duration-300 hover:bg-blue-500 hover:text-white bg-blue-100 w-8 h-8 px-2 rounded-full text-blue-400 cursor-pointer">
                                            <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap='round' strokeLinejoin='round' className="css-i6dzq1"><path d="M30.148 5.588c-2.934-3.42-7.288-5.588-12.148-5.588-8.837 0-16 7.163-16 16s7.163 16 16 16c4.86 0 9.213-2.167 12.148-5.588l-10.148-10.412 10.148-10.412zM22 3.769c1.232 0 2.231 0.999 2.231 2.231s-0.999 2.231-2.231 2.231-2.231-0.999-2.231-2.231c0-1.232 0.999-2.231 2.231-2.231z"></path><circle cx="12" cy="10" r="3"></circle></svg>
                                        </span>
                                    </div>
                                    <button className="flex items-center py-2 px-4 rounded-lg text-sm bg-blue-600 text-white shadow-lg">Enviar 
                                        <svg className="ml-1" viewBox="0 0 24 24" width="16" height="16" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap='round' strokeLinejoin='round'><line x1="22" y1="2" x2="11" y2="13"></line><polygon points="22 2 15 22 11 13 2 9 22 2"></polygon></svg>
                                    </button>
                                </footer>
                            </form>
                        </div>
                    }
                    {Com.Comentarios && Com.Comentarios.length > 0 &&
                        <div className="h-auto w-auto py-4 px-8">
                            <ComentariosItem
                                comentarios={Com.Comentarios}
                                IdCliente={IdCliente}
                                IdContenido={IdContenido}
                            />
                        </div>
                    }
                    </div></>
                ))}
            </>
        );
    } else {
        return(
            <>
            </>
        )
    }
    
}
