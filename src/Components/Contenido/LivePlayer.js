import React,{useState,useEffect} from "react";
import ReactPlayer from "react-player";


async function linkPaper(id) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        id:id
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/linkPaper',settings).then(data => data.json())    
}

export default function Player() {
  const iduser = sessionStorage.getItem("IdUsu")
  const idEvento = window.location.pathname.split('/')[3]
  const [video = "https://vimeo.com/31043983" ,SetVideo] = useState();
  useEffect(() => {
    linkPaper(idEvento).then(data => SetVideo(data))
  }, [])
  return (
    <div className="player-wrapper flex w-screen items-center justify-center aspect-video py-1">
      <ReactPlayer
        url={video}
        style={{ pointerEvents: 'none' }}
        controls={false}
      />
    </div>
  );
}
