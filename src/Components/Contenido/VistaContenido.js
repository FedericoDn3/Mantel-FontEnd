import React ,{useState,useEffect} from "react";
import ReproductorContenido from "./ReproductorContenido"
import ComentariosComponent from "./Comentario"
import Calificacion from "./Calificacion"
import PPVListado from "../Cliente/PPVListado"


async function GetContenido(id) {
    //Headers y body para el fetch.
    const settings = {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
          },
        body: new URLSearchParams({
          id:id
        })
    }
    return fetch('http://localhost:8080/Java2022_war/Prueba/traerContenido',settings).then(data => data.json())    
}

async function agregarFavorito(id, iduser) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        idContenido:id,
        id:iduser
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/agregarFavorito',settings).then(data => data.json())    
}

async function GetNext(id) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        idContenido:id
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/siguienteCapitulo',settings).then(data => data.json())    
}

async function CopyURL() {
  navigator.clipboard.writeText("http://localhost:3000" + window.location.pathname);
  alert("Link en porta papeles: " + "http://localhost:3000" + window.location.pathname);
}

async function GetDataEspecial(id, iduser) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        idContenido:id,
        id:iduser
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/getPuntajeLike',settings).then(data => data.json())    
}

async function reportarContenido(id, iduser) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        idContenido:id,
        id:iduser
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/reportarContenido',settings).then(data => data.json())    
}

export default function VistaContenido () {
    const [contenido = {
      id: 3,
      titulo: "x",
      imagen: "https://i.pinimg.com/originals/c5/fb/c9/c5fbc9d2104c4f8775d74823c492aa01.jpg",
      Destacado:0,
      Reportes:0,
      puntaje: 0,
      descripcion:'',
      Vistas: 0,
      Tipo: "Peli",
      comentarios:[],
      categorias:[],
      Estado: "Activo",
    } ,SetContenido] = useState();
    const [reportado = false ,SetReportado] = useState();
    const [like = false ,SetLike] = useState();
    const [puntaje = false ,SetPuntaje] = useState();
    const iduser = sessionStorage.getItem("IdUsu")
    const idPeli = window.location.pathname.split('/')[2]
    useEffect(() => {
      GetContenido(idPeli).then(data => SetContenido(data))
      GetDataEspecial(idPeli,iduser).then(data => {
        SetLike(data.like)
        SetPuntaje(data.puntaje)
      })
    }, [])
    return (
        <>
          <div className="flex justify-center">
            <div className="py-4 px-8 bg-MainC md:w-3/4 flex-row shadow-lg rounded-lg mx-14 my-20">
              <div>
                <h2 className="text-blue-900 flex justify-center text-3xl font-semibold">{contenido.titulo}</h2>
              </div>
              <div className="md:flex sm:flex-col-2 justify-start px-8">
                  <img className="flex w-24 pt-1 object-cover" src={contenido.imagen}  alt="Portada" style={{height:'25vh', width:'15vh'}} />
                  <p className="mt-2 px-8 text-white text-xs">{contenido.descripcion}</p>
              </div>
              <div className="bg-MainC flex-col rounded-lg w-48 text-gray-900 text-sm font-medium">
                <a aria-current="true" className="block px-4 py-2 w-full rounded-t-lg bg-MainC text-white cursor-pointer">Pais: {contenido.pais}</a>
                {/* <a aria-current="true" className="block px-4 py-2 w-full rounded-t-lg bg-MainC text-white cursor-pointer">Año: {contenido.pais}</a> */}
                <a aria-current="true" className="block px-4 py-2 w-full rounded-t-lg bg-MainC text-white cursor-pointer">Director: {contenido.director}</a>
                <a aria-current="true" className="block px-4 py-2 w-full rounded-t-lg bg-MainC text-white cursor-pointer">Idioma original: {contenido.idiomaOri}</a>
                <a aria-current="true" className="block px-4 py-2 w-full rounded-t-lg bg-MainC text-white cursor-pointer">Cla. Edad: {contenido.clasificacionEdad}</a>
              </div>
              <div className="flex justify-end mt-4">
              {contenido.categorias.map((Cat) => (
                <>
                  <a href={`http://localhost:3000/Buscar/${Cat.nombre}`} className="text-xl font-medium text-indigo-500 px-3">{Cat.nombre}</a> <br/>
                </>))}
              </div>
            </div>
          </div>
          <div className="flex bg-gray-300 w-auto  justify-center">
              <div className="h-4/5 md:w-2/5 w-auto py-4 px-8 bg-white shadow-lg rounded-lg my-20">
                  {contenido.tipoContenido === "Series" &&
                  <header className="flex justify-end mb-5">
                  <div className="flex gap-2">
                        <a href={`http://localhost:3000/BuscarSeries/${contenido.titulo}`}>Episodios</a>
                    </div>
                    <div className="flex gap-2">
                      <span className="flex items-center transition ease-out duration-300 hover:bg-pink-500 hover:text-white bg-blue-100 w-8 h-8 px-2 rounded-full text-blue-400 cursor-pointer">
                          <span className="flex items-center transition ease-out duration-300 hover:bg-red-500 hover:text-white bg-blue-100 w-8 h-8 px-2 rounded-full text-blue-400 cursor-pointer">
                            <svg onClick={() =>  GetNext(contenido.id)} viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                              <path d="M31 16l-15-15v9h-16v12h16v9z"></path>
                            </svg>
                          </span>
                        </span>
                        </div>
                  </header>}
                  <ReproductorContenido Video ={contenido.archivo}/>
                  <footer className="flex justify-between mt-2">
                    <div className="flex gap-2">
                      <div className="flex">
                      {!reportado &&
                        <span className="flex items-center transition ease-out duration-300 hover:bg-red-500 hover:text-white bg-blue-100 w-8 h-8 px-2 rounded-full text-blue-400 cursor-pointer">
                          <svg onClick={() => SetReportado(reportarContenido(contenido.id,iduser))} viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M0 0h4v32h-4v-32z"></path>
                            <path d="M26 20.094c2.582 0 4.83-0.625 6-1.547v-16c-1.17 0.922-3.418 1.547-6 1.547s-4.83-0.625-6-1.547v16c1.17 0.922 3.418 1.547 6 1.547z"></path>
                            <path d="M19 1.016c-1.466-0.623-3.61-1.016-6-1.016-3.012 0-5.635 0.625-7 1.547v16c1.365-0.922 3.988-1.547 7-1.547 2.39 0 4.534 0.393 6 1.016v-16z"></path>
                          </svg>
                        </span>
                      }
                      {reportado &&
                        <span className="flex items-center transition ease-out duration-300 hover:bg-red-500 hover:text-white bg-red-100 w-8 h-8 px-2 rounded-full text-red-400 cursor-pointer">
                        <svg onClick={() => SetReportado(reportarContenido(contenido.id,iduser))} viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                          <path d="M0 0h4v32h-4v-32z"></path>
                          <path d="M26 20.094c2.582 0 4.83-0.625 6-1.547v-16c-1.17 0.922-3.418 1.547-6 1.547s-4.83-0.625-6-1.547v16c1.17 0.922 3.418 1.547 6 1.547z"></path>
                          <path d="M19 1.016c-1.466-0.623-3.61-1.016-6-1.016-3.012 0-5.635 0.625-7 1.547v16c1.365-0.922 3.988-1.547 7-1.547 2.39 0 4.534 0.393 6 1.016v-16z"></path>
                        </svg>
                      </span>
                      }
                      </div>
                        <span className="flex items-center transition ease-out duration-300 hover:bg-blue-500 hover:text-white bg-blue-100 w-8 h-8 px-2 rounded-full text-blue-400 cursor-pointer">
                          <svg onClick={() => CopyURL()} viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M27 22c-1.411 0-2.685 0.586-3.594 1.526l-13.469-6.734c0.041-0.258 0.063-0.522 0.063-0.791s-0.022-0.534-0.063-0.791l13.469-6.734c0.909 0.94 2.183 1.526 3.594 1.526 2.761 0 5-2.239 5-5s-2.239-5-5-5-5 2.239-5 5c0 0.269 0.022 0.534 0.063 0.791l-13.469 6.734c-0.909-0.94-2.183-1.526-3.594-1.526-2.761 0-5 2.239-5 5s2.239 5 5 5c1.411 0 2.685-0.586 3.594-1.526l13.469 6.734c-0.041 0.258-0.063 0.522-0.063 0.791 0 2.761 2.239 5 5 5s5-2.239 5-5c0-2.761-2.239-5-5-5z"/>
                          </svg>
                        </span>
                        {like &&
                          <span className="flex items-center transition ease-out duration-300 hover:bg-pink-500 hover:text-white bg-pink-100 w-8 h-8 px-2 rounded-full text-pink-400 cursor-pointer">
                            <svg onClick={() => agregarFavorito(contenido.id,iduser)} viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                              <path d="M23.6 2c-3.363 0-6.258 2.736-7.599 5.594-1.342-2.858-4.237-5.594-7.601-5.594-4.637 0-8.4 3.764-8.4 8.401 0 9.433 9.516 11.906 16.001 21.232 6.13-9.268 15.999-12.1 15.999-21.232 0-4.637-3.763-8.401-8.4-8.401z"></path>
                            </svg>
                          </span>
                        }
                        {!like &&
                          <span className="flex items-center transition ease-out duration-300 hover:bg-pink-500 hover:text-white bg-blue-100 w-8 h-8 px-2 rounded-full text-blue-400 cursor-pointer">
                            <svg onClick={() => agregarFavorito(contenido.id,iduser)} viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                              <path d="M23.6 2c-3.363 0-6.258 2.736-7.599 5.594-1.342-2.858-4.237-5.594-7.601-5.594-4.637 0-8.4 3.764-8.4 8.401 0 9.433 9.516 11.906 16.001 21.232 6.13-9.268 15.999-12.1 15.999-21.232 0-4.637-3.763-8.401-8.4-8.401z"></path>
                            </svg>
                          </span>
                        }
                    </div>
                    <Calificacion Nota={contenido.puntaje} IdCliente={iduser}/>
                  </footer>
              </div>
          </div>
          <div className="flex h-auto w-auto justify-center">
            <div className="md:w-2/5 h-4/5 w-auto py-4 px-8 bg-white shadow-lg rounded-lg my-20">
                <ComentariosComponent comentarios={contenido.comentarios} IdCliente={iduser} IdContenido={contenido.id}/>
            </div>
          </div>
      </>
    );
}
