import { Result } from 'postcss';
import React ,{useState,useEffect} from 'react'
import { Link } from 'react-router-dom'

async function listarBusqueda(query  = "", filtro="") {
  var tipo = "busqueda"
  if (window.location.pathname.split('/')[1] === "Series" && query === null)
    tipo="listarSeries"
  if (window.location.pathname.split('/')[1] === "Peliculas")
    tipo="listarPeliculas"
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charSet=UTF-8'
        },
      body: new URLSearchParams({
        texto:query.replace(/%20/g, " "),
        filtro:filtro
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/'+tipo,settings).then(data => data.json())    
}
export default function BuscarSeries() {
  const titulo = window.location.pathname.split('/')[2].replace(/%20/g, " ")
  const [contenidos = [], setContenidos] = useState();
  useEffect(() => {
    listarBusqueda(titulo,"").then(data => setContenidos(data))
  }, [])
  return(<>
    <div className="bg-white p-8 rounded-md w-full">
      <div className=" flex items-center justify-between pb-6">
        <div>
          <h2 className="text-gray-600 font-semibold">{titulo}</h2>
        </div>
      </div>
      <div>
        <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
          <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
            <table className="min-w-full leading-normal">
              <thead>
                <tr>
                  <th
                    className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Nombre
                  </th>
                  <th
                    className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Tipo
                  </th>
                  <th
                    className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Calificación
                  </th>
                </tr>
              </thead>
              <tbody>
                {contenidos.map((C) => (<>
                  <tr key={C.id}>
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <Link to={`/Peli/${C.id}`}>
                      <div className="flex items-center">
                        <div className="flex-shrink-0 w-10 h-10">
                          <img className="w-full h-full rounded-full"
                            src={ C.imagen}
                            alt="" />
                        </div>
                        <div className="ml-3">
                          <p className="text-gray-900 whitespace-no-wrap">
                            {C.titulo}
                          </p>
                        </div>
                        <div className="ml-3">
                          <p className="text-gray-900 whitespace-no-wrap">
                            {C.numeroCap}
                          </p>
                        </div>
                      </div>
                      </Link>
                    </td>
                    
                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <p className="text-gray-900 whitespace-no-wrap">
                        {C.tipoContenido}
                      </p>
                    </td>
                    <td className="px-5 py-5 text-justify border-b border-gray-200 bg-white text-sm">
                      <div className=" flex gap-2" type="button" data-modal-toggle="default-modal">
                        {C.puntaje > 0 &&
                        <span type="button" data-modal-toggle="default-modal" className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                          <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                        {C.puntaje > 1 &&
                        <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                        {C.puntaje > 2 &&
                        <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                        {C.puntaje > 3 &&
                        <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                        {C.puntaje > 4 &&
                        <span className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                          <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                          </svg>
                        </span>}
                      </div>
                    </td>
                  </tr>
                  </>))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </>)
    
}

// export default function ListaBuscar() {
//   const query = window.location.pathname.split('/')[2]
//   const [contenidos = [], setContenidos] = useState();
//   useEffect(() => {
//     listarBusqueda(query).then(data => setContenidos(data))
//   }, [])
//   return (
//     <>
//       {contenidos && contenidos.length !== 0 &&
//         <div className="grid grid-cols-2 md:lg:xl:grid-cols-9 group bg-white shadow-xl shadow-neutral-100 border">
//         {contenidos.map((C) => (
//           <>
//             <div key={C.Id} className="bg-gray-100 flex justify-center items-center">
//               <Link to={`/Peli/${C.id}`}>
//                 <img className="flex w-24 pt-1 object-cover" src={  C.imagen}  alt="Portada" />
//               </Link>
//             </div>
//           </>
//         ))}
//       </div>  
//       }
//     </>
//   );
// }
