import FilePlayer from "react-player/file";
import React from "react";
import ReactPlayer from "react-player";




async function agregarVisualizacion(id,IdContenido,tiempoRep,termino) {
  //Headers y body para el fetch.
  const settings = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        },
      body: new URLSearchParams({
        IdCliente:id,
        IdContenido:IdContenido,
        tiempoRep:tiempoRep,
        termino:termino
      })
  }
  return fetch('http://localhost:8080/Java2022_war/Prueba/agregarVisualizacion',settings).then(data => data.json())    
}


export default function ReproductorContenido({Video}, tiempo) {
  const iduser=sessionStorage.getItem("IdUsu");
  return (
  <div className="md:2/5 player-wrapper h-4/5  flex items-center justify-center aspect-video py-1">
    <ReactPlayer
      url={Video}
      height={200}
      // onPause={()=>{agregarVisualizacion(iduser,idEvento,this.p.getDuration(),termino)}}
      // onProgress={Actualizar()}
      width={300}
      controls={true}
      //style={{ pointerEvents: 'none' }}
    />
  </div>
);
}
