import {useState} from 'react'
import ComentariosItem from "./ComentarioItem"

function CrearJsonComentarios(Comentarios, IDPadre=0) {
    let Result = []
    Comentarios.forEach(C => {
        if (C.idPadre === IDPadre) {
            let com = {
                "Id": C.id,
                "NombreAutor": C.nombre,
                "Comentario": C.comentario,
                "reportado":C.reportado,
                "Comentarios": CrearJsonComentarios(Comentarios, C.id)
              }
            Result.push(com)
        }
    });
    return Result
  }

  async function Comentar(C) {
    const settings = {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
          },
        body: new URLSearchParams(C)
    }
    return fetch('http://localhost:8080/Java2022_war/Prueba/comentarContenido',settings).then(data => data.json())    
  }
  
export default function ComentariosComponent({comentarios, IdCliente, IdContenido}) {
    const [comentario, setComentario] = useState();
    // const [spoiler=false, setSpoiler] = useState();
    const handleSubmit = async (e) => {
        e.preventDefault();
        const C = {
            idContenido:IdContenido,
            idPadre:0,
            idCliente:IdCliente,
            texto:comentario
        }
        Comentar(C)
    }
    return(<>
        <div>
        {/* <link rel="stylesheet" href="https://unpkg.com/@themesberg/flowbite@1.1.0/dist/flowbite.min.css" />
    
            <label onClick={() =>setSpoiler(!spoiler)} htmlFor="toggle-example" className="flex items-center cursor-pointer relative mb-4">
            <input type="checkbox" id="toggle-example" className="sr-only"/>
             <div className="toggle-bg bg-gray-200 border-2 border-gray-200 h-6 w-11 rounded-full"></div>
                <span className="ml-3 text-gray-900 text-sm font-medium">Ver spoilers</span>
            </label> */}
            <form onSubmit={handleSubmit} className="bg-white shadow rounded-lg mb-6 p-4">
                <textarea name="message" onChange={e => setComentario(e.target.value)} placeholder="Comenta algo" className="w-full rounded-lg p-2 text-sm bg-gray-100 border border-transparent appearance-none rounded-tg placeholder-gray-400"></textarea>
                <footer className="flex justify-between mt-2">
                    <button className="flex items-center py-2 px-4 rounded-lg text-sm bg-blue-600 text-white shadow-lg">Enviar 
                        <svg className="ml-1" viewBox="0 0 24 24" width="16" height="16" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap='round' strokeLinejoin='round'><line x1="22" y1="2" x2="11" y2="13"></line><polygon points="22 2 15 22 11 13 2 9 22 2"></polygon></svg>
                    </button>
                </footer>
            </form>
        </div>
        {comentarios && comentarios.length !== 0 &&
            <ComentariosItem comentarios={CrearJsonComentarios(comentarios,0)} IdCliente={IdCliente} IdContenido={IdContenido}/> 
        }
    </>)
}
