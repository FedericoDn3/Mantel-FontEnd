


async function Calificar(nota, IdCliente) {
    //Headers y body para el fetch.
    const idCont = window.location.pathname.split('/')[2]
    const settings = {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
          },
        body: new URLSearchParams({
            idContenido:idCont,
            id:IdCliente,
            puntaje:nota
        })
    }
    return fetch('http://localhost:8080/Java2022_war/Prueba/puntuarContenido',settings).then(data => data.json())    
}

export default function Calificacion({Nota, IdCliente}) {
        return (
            <>
               <div className="flex gap-2" type="button" data-modal-toggle="default-modal">
                    {Nota > 0 &&
                    <span onClick={() => Calificar(1,IdCliente)} type="button" data-modal-toggle="default-modal" className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                        <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota > 1 &&
                    <span onClick={() => Calificar(2,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota > 2 &&
                    <span onClick={() => Calificar(3,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota > 3 &&
                    <span onClick={() => Calificar(4,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300 cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota > 4 &&
                    <span onClick={() => Calificar(5,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300  cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="currentColor" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota < 5 &&
                    <span onClick={() => Calificar(1,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300  cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota < 4 &&
                    <span onClick={() => Calificar(2,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300  cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota < 3 &&
                    <span onClick={() => Calificar(3,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300  cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota < 2 &&
                    <span onClick={() => Calificar(4,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300  cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                    {Nota < 1 &&
                    <span onClick={() => Calificar(5,IdCliente)} className="flex items-center transition ease-out duration-300 w-8 h-8 px-2 text-yellow-300  cursor-pointer">
                        <svg  viewBox="0 0 32 32" width="24" height="24" stroke="currentColor" strokeWidth={2} fill="none" strokeLinecap="round" strokeLinejoin="round" className="css-i6dzq1">
                            <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
                        </svg>
                    </span>}
                </div>
            </>
        );
}
