import React from "react";

import useChat from "./useChat";

const Chat = (props) => {
  const M = window.location.pathname.split('/')[2].replace(/%20/g, " ")
  const { messages, sendMessage } = useChat(M); // Creates a websocket and manages messaging
  const [newMessage, setNewMessage] = React.useState(""); // Message to be sent

  const handleNewMessageChange = (event) => {
    setNewMessage(event.target.value);
  };

  const handleSendMessage = () => {
    sendMessage(sessionStorage.getItem("Nombre")+" : " + newMessage);
    setNewMessage("");
  };

  return (
    <div className="chat-room-container max-w-lg max-h-lg flex-col  m-4">
      <h1 className="room-name my-0  py-1">{M}</h1>
      <div className="messages-container flex-1 min-h-0 h-3/4 overflow-auto border-2 my-2 border-black rounded-t-md">
        <ol className="messages-list .my-message ">
          {messages.map((message, i) => (
            <li
              key={i}
              className={`message-item flex py-2 ${
                message.ownedByCurrentUser ? "my-message bg-gradient-to-l from-MainC justify-end rounded-lg"  : "received-message bg-gradient-to-r from-orange-500 rounded-lg "
              }`}
            > 
              {message.body}
            </li>
          ))}
        </ol>
      </div>
      <textarea
        value={newMessage}
        onChange={handleNewMessageChange}
        placeholder="Escribe Algo..."
        className="new-message-input-field h-12 max-h-max w-full border-2 border-black "
      />
      <button onClick={handleSendMessage} className="send-message-button border-2 rounded-lg py-2 px-2 border-black">
        Enviar
      </button>
    </div>
  );
};

export default Chat;