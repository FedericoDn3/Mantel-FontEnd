module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    fontFamily:{
      Cc:["Cc","sans-serif"]
    },

    extend: {
      colors: {
        'MainC': '#1AA7EC',
        //'MainC': '#0df310',
        //'CineCalidad':'#42271c'
        'CineCalidad':'#000000',
        //'SecC': '#f5e903',
        'SecC': '#f5e903'

      },
    }
  },
  plugins: [],
}